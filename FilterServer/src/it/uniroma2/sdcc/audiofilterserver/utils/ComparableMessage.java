package it.uniroma2.sdcc.audiofilterserver.utils;


import it.uniroma2.sdcc.audiofilterserver.threads.MainThread;

import java.util.Map;

import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;

public class ComparableMessage implements Comparable<ComparableMessage>{
	public Message msg;

	public ComparableMessage(Message msg){
		this.msg = msg;
	}

	@Override
	public int compareTo(ComparableMessage cmsg){
		Message msg = cmsg.msg;
		Map<String, MessageAttributeValue> myAttr = this.msg.getMessageAttributes();
		Map<String, MessageAttributeValue> itsAttr = msg.getMessageAttributes();
		int myChunk = Integer.parseInt(myAttr.get(MainThread.ChunkTag).getStringValue());
		int itsChunk = Integer.parseInt(itsAttr.get(MainThread.ChunkTag).getStringValue());
		return myChunk - itsChunk;
	}
}