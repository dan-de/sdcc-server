package it.uniroma2.sdcc.audiofilterserver.utils;

import it.uniroma2.sdcc.audiofilterserver.threads.MainThread;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.DeleteItemResult;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;
import com.amazonaws.services.dynamodbv2.model.UpdateItemResult;

public class DynDBUsers {
	
	AmazonDynamoDBClient addc;
	String tableName;
	
	public static final String RoomColumn 				= "Room Name";
	public static final String UserColumn 				= "User ID";
	public static final String LastFilteredChunkColumn 	= "Last Chunk";
	public static final String UserUrlColumn 			= "User Url";
	
	public DynDBUsers(String tableName) {
		this.tableName = tableName;
		initDB();
	}
	
	public void initDB(){
		addc = null;
		try{
			addc = new AmazonDynamoDBClient(MainThread.credentials);
			if(MainThread.Regs!=null)
				addc.setRegion(Region.getRegion(MainThread.Regs));
			else
				addc.setRegion(Regions.getCurrentRegion());
		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which means your request made it " +
					"to Amazon SQS, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered " +
					"a serious internal problem while trying to communicate with SQS, such as not " +
					"being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
	}
	
	public UpdateItemResult updateItem(Map<String, AttributeValue> key, Map<String, AttributeValueUpdate> attributeUpdates){
		return addc.updateItem(tableName, key, attributeUpdates);
	}
	
	public GetItemResult readItem(Map<String, AttributeValue> key, boolean consistentRead){
		return addc.getItem(tableName, key, consistentRead);
	}
	
	public List<Map<String, AttributeValue>> returnRoomEntry(String roomName){
		Map<String, Condition> condition = new HashMap<String, Condition>();
		Condition c = new Condition();
		c.setComparisonOperator(ComparisonOperator.EQ);
		
		List<AttributeValue> set = new ArrayList<AttributeValue>();
		set.add(new AttributeValue().withS(roomName));
		c.setAttributeValueList(set);
		condition.put(RoomColumn, c);
		QueryRequest qr = new QueryRequest(tableName).withKeyConditions(condition);
		QueryResult qrs = addc.query(qr);
		return qrs.getItems();	
	}
	
	public PutItemResult addItem(Map<String, AttributeValue> item){
		return addc.putItem(tableName, item);
	}
	
	public DeleteItemResult removeItem(Map<String, AttributeValue> item){
		return addc.deleteItem(tableName, item);
	}
}
