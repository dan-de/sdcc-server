package it.uniroma2.sdcc.audiofilterserver.utils;
import it.uniroma2.sdcc.audiofilterserver.threads.MainThread;

import java.nio.ByteBuffer;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;


public class SendMessage {
	
	public static final String FileTag 	= "FileName";
	public static final String ChunkTag = "ChunkId";
	public static final String FlagsTag = "Flags";
	public static final String DataTag = "Data";
	
	public static void sendMessage(String url, String fileName, int chunkId, int flags, ByteBuffer body){
		AmazonSQS sqs = new AmazonSQSClient(MainThread.credentials);
		if(MainThread.Regs!=null)
			sqs.setRegion(Region.getRegion(MainThread.Regs));
		else
			sqs.setRegion(Regions.getCurrentRegion());
        
     // Send a message
        System.out.println("Sending a message to the queue.\n");
        SendMessageRequest smr = new SendMessageRequest();
        MessageAttributeValue mav = new MessageAttributeValue();
        
        smr.setQueueUrl(url);
        
        mav.setStringValue(fileName);
        mav.setDataType("String");
        smr.addMessageAttributesEntry(FileTag, mav);
        mav = new MessageAttributeValue();
        mav.setStringValue(Integer.toString(chunkId));
        mav.setDataType("Number");
        smr.addMessageAttributesEntry(ChunkTag, mav);
        mav = new MessageAttributeValue();
        mav.setStringValue(Integer.toString(flags));
        mav.setDataType("Number");
        smr.addMessageAttributesEntry(FlagsTag, mav);
        mav = new MessageAttributeValue();
        mav.setBinaryValue(body);
        mav.setDataType("Binary");
        smr.addMessageAttributesEntry(DataTag, mav);
        
        smr.setMessageBody("Message with id "+chunkId+". Flag: "+flags);
        
        sqs.sendMessage(smr);
	}
}
