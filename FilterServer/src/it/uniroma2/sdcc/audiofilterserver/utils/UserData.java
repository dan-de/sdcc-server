package it.uniroma2.sdcc.audiofilterserver.utils;
import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;


public class UserData {
	private int lastOrderedChunk;
	private int lastFilteredChunk;
	private int firstOutOfOrderChunk;
	private PriorityBlockingQueue<ComparableMessage> ordered;
	private PriorityBlockingQueue<ComparableMessage> outOfOrder;
	
	public UserData(){
		lastOrderedChunk 		= -1;
		lastFilteredChunk 		= -1;
		firstOutOfOrderChunk 	= Integer.MAX_VALUE;
		ordered 				= new PriorityBlockingQueue<ComparableMessage>();
		outOfOrder				= new PriorityBlockingQueue<ComparableMessage>();
	}
	
	public void offerOrdered(ComparableMessage m){
		ordered.offer(m);
	}
	public ComparableMessage pollOrdered(){
		return ordered.poll();
	}
	public ComparableMessage peekOrdered(){
		return ordered.peek();
	}
	public ComparableMessage peekIndexedOrdered(int index) throws InterruptedException{
		ArrayList<ComparableMessage> msgs = new ArrayList<ComparableMessage>();
		ComparableMessage cmsg;
		for(int i=0;i<index;i++){
			cmsg = ordered.take();
			msgs.add(cmsg);
		}
		cmsg = ordered.take();
		for(ComparableMessage c : msgs){
			ordered.offer(c);
		}
		ordered.offer(cmsg);
		return cmsg;
	}
	
	public void offerOutOfOrder(ComparableMessage m){
		outOfOrder.offer(m);
	}
	public ComparableMessage pollOutOfOrder(){
		return outOfOrder.poll();
	}
	public ComparableMessage peekOutOfOrder(){
		return outOfOrder.peek();
	}
	
	public int getLastOrderedChunk() {
		return lastOrderedChunk;
	}
	public void setLastOrderedChunk(int lastOrderedChunk) {
		this.lastOrderedChunk = lastOrderedChunk;
	}
	public int getFirstOutOfOrderChunk() {
		return firstOutOfOrderChunk;
	}
	public void setFirstOutOfOrderChunk(int firstOutOfOrderChunk) {
		this.firstOutOfOrderChunk = firstOutOfOrderChunk;
	}
	public PriorityBlockingQueue<ComparableMessage> getOrdered() {
		return ordered;
	}
	public void setOrdered(PriorityBlockingQueue<ComparableMessage> ordered) {
		this.ordered = ordered;
	}
	public int getLastFilteredChunk() {
		return lastFilteredChunk;
	}

	public void setLastFilteredChunk(int lastFilteredChunk) {
		this.lastFilteredChunk = lastFilteredChunk;
	}

	public PriorityBlockingQueue<ComparableMessage> getOutOfOrder() {
		return outOfOrder;
	}
	public void setOutOfOrder(PriorityBlockingQueue<ComparableMessage> outOfOrder) {
		this.outOfOrder = outOfOrder;
	}
}
