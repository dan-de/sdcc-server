package it.uniroma2.sdcc.audiofilterserver.threads;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;

public class UserThread extends Thread {
	
	private String urlQueue;
	private String userName;
	
	public boolean cont;
	
	private long timestamp;
	
	public UserThread(String urlQueue, String userName){
		this.urlQueue = MainThread.correctUrl(urlQueue);
		this.userName =  userName;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUrlQueue() {
		return urlQueue;
	}

	public void setUrlQueue(String urlQueue) {
		this.urlQueue = urlQueue;
	}

	public void run(){
		readingOrdered();
	}
	
	private void readingOrdered(){
		System.out.println("Receiving messages from "+userName+".\n");
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(urlQueue).withAttributeNames("All").withMessageAttributeNames(new String[]{MainThread.ChunkTag, MainThread.FlagsTag, MainThread.DataTag, MainThread.TimeTag});
		receiveMessageRequest.setMaxNumberOfMessages(10);
		cont = true;
		boolean check = true;
		long timer = 0, startTimer = System.currentTimeMillis();
		orderer:while(cont){
			if(timer > 60000){
				// Timeout. Create a fake close message
				Message msg = new Message();
				Map<String, MessageAttributeValue> messageAttributes = new HashMap<String, MessageAttributeValue>();
				MessageAttributeValue mav = new MessageAttributeValue();
				mav.setStringValue(Integer.toString(MainThread.timeoutSignal));
				mav.setDataType("Number");
				messageAttributes.put(MainThread.FlagsTag, mav);
				
				int chunkID = MainThread.perUserData.get(userName).getLastOrderedChunk()+1;
				mav = new MessageAttributeValue();
				mav.setStringValue(Integer.toString(chunkID));
				mav.setDataType("Number");
				messageAttributes.put(MainThread.ChunkTag, mav);
				
				mav = new MessageAttributeValue();
				mav.setStringValue(Long.toString(timestamp+MainThread.timestampSecondThresh));
				mav.setDataType("Number");
				messageAttributes.put(MainThread.TimeTag, mav);
				
				ByteBuffer data = ByteBuffer.allocate(4);
				data.putInt(-1);
				data.rewind();
				mav = new MessageAttributeValue();
				mav.setBinaryValue(data);
				mav.setDataType("Binary");
				messageAttributes.put(MainThread.DataTag, mav);
				msg.setMessageAttributes(messageAttributes);
				
				MainThread.addData(userName, msg);

				break orderer;
			}
			List<Message> messages;
			try{
				messages = MainThread.sqs.receiveMessage(receiveMessageRequest).getMessages();
			}
			catch (Exception e){
				System.err.println("UserThread: problem with "+urlQueue);
				MainThread.ulock.readLock().lock();
				cont = false;
				MainThread.ulock.readLock().unlock();
				continue;
			}
			timer = System.currentTimeMillis() - startTimer;
			for(Message i : messages){
				Map<String, MessageAttributeValue> attr = i.getMessageAttributes();
				long timestamp = Long.parseLong(attr.get(MainThread.TimeTag).getStringValue());
				if(this.timestamp<timestamp){
					this.timestamp = timestamp;
				}
				timer = 0;
				startTimer = System.currentTimeMillis();
				MainThread.rwlock.readLock().lock();
				check = MainThread.addData(userName, i);
				MainThread.rwlock.readLock().unlock();
				if(!check){
					MainThread.ulock.readLock().lock();
					cont = false;
					MainThread.ulock.readLock().unlock();
				}
			}
		}
	}
}
