package it.uniroma2.sdcc.audiofilterserver.threads;

import it.uniroma2.sdcc.audiofilterserver.utils.DynDBUsers;
import it.uniroma2.sdcc.audiofilterserver.utils.UserData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;

public class ServiceThread extends Thread {
	private String serviceQueueUrl;
	public static final String UrlTag 	= "Url";
	public static final String NameTag	= "User";
	
	public static int WaitSeconds = 1;
	
	protected volatile boolean running = true;
	
	public ServiceThread(String sqsUrl){
		serviceQueueUrl = sqsUrl;
	}
	
	public void run(){
		System.out.println("ServiceThread: starting");
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(serviceQueueUrl).withAttributeNames("All").withMessageAttributeNames(new String[]{NameTag, UrlTag});
		receiveMessageRequest.setMaxNumberOfMessages(10);
		receiveMessageRequest.setWaitTimeSeconds(WaitSeconds);
		while(running){
			List<Message> messages = MainThread.sqs.receiveMessage(receiveMessageRequest).getMessages();
			if(!messages.isEmpty()){
				for(Message i : messages){
					System.out.println("ServiceThread: received new message");
					Map<String, MessageAttributeValue> attr = i.getMessageAttributes();
					String url 	= attr.get(UrlTag).getStringValue();
					String name = url.substring(url.lastIndexOf("/")+1);

					Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
					item.put(DynDBUsers.RoomColumn, new AttributeValue("String").withS(MainThread.roomName));
					item.put(DynDBUsers.UserColumn, new AttributeValue("String").withS(name));
					item.put(DynDBUsers.UserUrlColumn, new AttributeValue("String").withS(url));
					item.put(DynDBUsers.LastFilteredChunkColumn, new AttributeValue().withN(Integer.toString(Integer.MIN_VALUE)));
					MainThread.addc.addItem(item);


					String queueName = url.substring(url.lastIndexOf("/")+1);
					String queueUrl = MainThread.sqs.getQueueUrl(queueName).getQueueUrl();
					UserThread ut = new UserThread(queueUrl, name);

					try{
						MainThread.rwlock.writeLock().lock();
						ut.start();
						MainThread.perUserData.put(name, new UserData());
						MainThread.urls.put(name, queueUrl);
						MainThread.uThreadPool.put(name, ut);

					}catch(Exception e){
						System.exit(1);
					}finally{
						MainThread.rwlock.writeLock().unlock();
					}
					String receipt = i.getReceiptHandle();
					MainThread.sqs.deleteMessage(serviceQueueUrl, receipt);
				}
			}
		}
		System.out.println("ServiceThread: stopping");
		if(!MainThread.NoDelete)
			MainThread.sqs.deleteQueue(serviceQueueUrl);
	}

    public void terminate() {
    	MainThread.ulock.readLock().lock();
        running = false;
        MainThread.ulock.readLock().unlock();
    }


}
