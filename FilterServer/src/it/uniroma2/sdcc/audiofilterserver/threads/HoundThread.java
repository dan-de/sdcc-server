package it.uniroma2.sdcc.audiofilterserver.threads;

import it.uniroma2.sdcc.audiofilterserver.utils.DynDBServers;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;

public class HoundThread extends Thread {
	
	String roomName;
	String serviceUrl;
	String savingUrl;
	
	private volatile boolean running 	= true;
	public static long WaitTime 		= 7000; 
	public static final int	flagServer 	= 1;
	
	public HoundThread(String roomName, String serviceUrl, String savingUrl){
		this.roomName = roomName;
		this.serviceUrl = serviceUrl;
		this.savingUrl = savingUrl;
		
		constructItem();
	}
	
	public void run(){
		System.out.println("HoundThread: starting. Updates every "+WaitTime+" ms");
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put(DynDBServers.RoomColumn, new AttributeValue().withS(roomName));
		key.put(DynDBServers.TypeColumn, new AttributeValue().withN(Integer.toString(flagServer)));
		while(running){
			try {
				long time = System.currentTimeMillis();
				if(MainThread.ulock.writeLock().tryLock(WaitTime-2000, TimeUnit.MILLISECONDS)){
					Set<String> set = MainThread.uThreadPool.keySet();
					for(String u : set){
						UserThread ut = MainThread.uThreadPool.get(u);
						if(ut.cont){
							if(!ut.isAlive()){
								System.err.println("HoundThread: UserThread crashed");
								System.exit(1);
							}
						}
						else{
							ut.setDaemon(true);
							MainThread.uThreadPool.remove(u);
						}
					}
					MainThread.ulock.writeLock().unlock();
				}
				time = System.currentTimeMillis() - time;
				if(WaitTime-time>0)
					Thread.sleep(WaitTime-time);
			} catch (InterruptedException e) {
				if(MainThread.ulock.isWriteLockedByCurrentThread())
					MainThread.ulock.writeLock().unlock();
				e.printStackTrace();
			}
			GetItemResult gir = MainThread.dds.readItem(key, false);
			if(gir.getItem() != null){
				Map<String, AttributeValueUpdate> update = new HashMap<String, AttributeValueUpdate>();
				AttributeValue av =  new AttributeValue().withN(Long.toString(System.currentTimeMillis()));
				AttributeValueUpdate avu = new AttributeValueUpdate().withAction("PUT").withValue(av);
				update.put(DynDBServers.TimestampColumn, avu);
				MainThread.dds.updateItem(key, update);
			}
			else{
				System.out.println("HoundThread: No item with the given key. Creating...");
				constructItem();
			}
		}
		System.out.println("HoundThread: stopping");
		MainThread.dds.removeItem(key);
	}
	
	private void constructItem(){
		Map<String, AttributeValue> row = new HashMap<String, AttributeValue>();
		row.put(DynDBServers.RoomColumn, new AttributeValue().withS(roomName));
		row.put(DynDBServers.UrlServiceColumn, new AttributeValue().withS(serviceUrl));
		row.put(DynDBServers.UrlSavingColumn, new AttributeValue().withS(savingUrl));
		row.put(DynDBServers.TimestampColumn, new AttributeValue().withN(Long.toString(System.currentTimeMillis())));
		row.put(DynDBServers.TypeColumn, new AttributeValue().withN(Integer.toString(flagServer)));
		MainThread.dds.addItem(row);
	}

    public void terminate() {
        running = false;
    }

}
