package it.uniroma2.sdcc.audiofilterserver.threads;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import it.uniroma2.sdcc.audiofilterserver.audioclean.CleaningAlgorithm;
import it.uniroma2.sdcc.audiofilterserver.utils.ComparableMessage;
import it.uniroma2.sdcc.audiofilterserver.utils.DynDBRooms;
import it.uniroma2.sdcc.audiofilterserver.utils.DynDBServers;
import it.uniroma2.sdcc.audiofilterserver.utils.DynDBUsers;
import it.uniroma2.sdcc.audiofilterserver.utils.RESTException;
import it.uniroma2.sdcc.audiofilterserver.utils.RESTRequest;
import it.uniroma2.sdcc.audiofilterserver.utils.SendMessage;
import it.uniroma2.sdcc.audiofilterserver.utils.UserData;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;

public class MainThread extends Thread{
	
	public static AWSCredentials credentials;
	
	protected static ConcurrentHashMap<String, UserData> perUserData;
	protected static ConcurrentHashMap<String, Integer> peeked;
	protected static ConcurrentHashMap<String, String> urls;
	protected static ConcurrentHashMap<String, UserThread> uThreadPool;
	protected static ReentrantReadWriteLock ulock;
	protected static ReentrantReadWriteLock rwlock;
	protected static AmazonSQS sqs;
	protected static DynDBUsers addc;
	protected static DynDBRooms ddr;
	protected static DynDBServers dds;
	
	/* Setting files information */
	private static final String propertiesFile = System.getProperty("user.home")+"/.sdcc/settingsFilter.txt";
	public static Regions Regs;
	public static boolean NoDelete;

	private static long timeout = 300000;

	private static int msgToClean;
	
	/*
	 * HoundThread.WaitTime
	 * ServiceThread.WaitSeconds
	 * RESTRequest.token
	 */
	
	/* **** */
	
	protected static int sent;
	
	public static final String UsersTableName	= "CodeP_Users";
	public static final String RoomsTableName 	= "CodeP_Rooms";
	public static final String ServersTableName = "CodeP_Servers";
	
	public static final String TokenTag 	= "Token";
	public static final String ChunkTag 	= "ChunkId";
	public static final String FlagsTag 	= "Flags";
	public static final String DataTag 		= "Data";
	public static final String TimeTag 		= "Timestamp";
	public static final int closeSignal 	= 1;
	public static final int timeoutSignal 	= 2;
	
	protected static String roomName;
	protected static String serviceQueueUrl;
	protected static String savingQueueUrl;
	
	private static Long oldTimestamp;
	
	private boolean started;
	private long oldTimer;
	
	private static final int maxMsgDim	= 261120;
	protected static long timestampSecondThresh = 0;
	
	public static ServiceThread st;
	public static HoundThread ht;

	private static Properties settings;
	
	/**
	 * Constructor. Takes main arguments
	 * @param args: main arguments
	 */
	public MainThread(String[] args){
		System.out.println("MainThread: initialization");
		
		/* Setting up AWS objects and other classes related */
		System.out.println("MainThread: setting up AWS Services");
		credentials = null;
		try {
			credentials = new ProfileCredentialsProvider("default").getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException(
					"Cannot load the credentials from the credential profiles file. " +
							"Please make sure that your credentials file is at the correct " +
							"location (/home/daniele/.aws/credentials), and is in valid format.",
							e);
		}
		
		try{
			sqs = new AmazonSQSClient(credentials);
			if(Regs!=null)
				sqs.setRegion(Region.getRegion(Regs));
			else
				sqs.setRegion(Regions.getCurrentRegion());
			addc = new DynDBUsers(UsersTableName);
			ddr = new DynDBRooms(RoomsTableName);
			dds = new DynDBServers(ServersTableName);
		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which means your request made it " +
					"to Amazon SQS, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
			System.exit(1);
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered " +
					"a serious internal problem while trying to communicate with SQS, such as not " +
					"being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
			System.exit(1);
		}
		roomName = args[0];
		serviceQueueUrl = correctUrl(args[1]);
		savingQueueUrl = correctUrl(args[2]);
		
		rwlock = new ReentrantReadWriteLock(true);
		ulock = new ReentrantReadWriteLock(true);
		
		perUserData = new ConcurrentHashMap<String, UserData>();
		peeked = new ConcurrentHashMap<String, Integer>();
		urls = new ConcurrentHashMap<String, String>();
		uThreadPool = new ConcurrentHashMap<String, UserThread>();
		sent = 0;
		
		oldTimestamp = (long) 0;
		
		/* State resume after a crash */
		resumeState();
		
		/* Start of the service communications thread */
		st = new ServiceThread(serviceQueueUrl);
		st.start();
		
		/* Start of the anti-watchdog thread */
		ht = new HoundThread(roomName, serviceQueueUrl, savingQueueUrl);
		try{
			ht.setPriority(ht.getPriority()+1);
		}
		catch(Exception e){
			System.err.println("MainThread: Impossible to set a new priority");
		}
		ht.start();
	}
	
	/**
	 * Resume state after a crash. If the server is started for the first time, it has no effect
	 */
	public void resumeState(){
		System.out.println("MainThread: resuming eventual previous state");
		
		/* Get all the users in the room */
		List<Map<String, AttributeValue>> users = addc.returnRoomEntry(roomName);
		if(users.isEmpty()){
			/* 0 users: new start */
			System.out.println("MainThread: nothing to resume");
			return;
		}
		rwlock.writeLock().lock();
		
		/* For each user restore the data structures: retrieve queue url and last filtered chunk */
		for(Map<String, AttributeValue> m : users){
			UserData ud = new UserData();
			AttributeValue av =  m.get(DynDBUsers.LastFilteredChunkColumn);
			ud.setLastFilteredChunk(Integer.parseInt(av.getN()));
			ud.setLastOrderedChunk(ud.getLastFilteredChunk());
			
			String uname = m.get(DynDBUsers.UserColumn).getS();
			perUserData.put(uname, ud);
			
			av = m.get(DynDBUsers.UserUrlColumn);
			urls.put(uname, correctUrl(av.getS()));
			UserThread ut = new UserThread(av.getS(),uname);
			ut.start();
			uThreadPool.put(uname, ut);
		}
		rwlock.writeLock().unlock();
		
		/* Restore the filter counter of messages sent */
		Map<String, AttributeValue> room = new HashMap<String, AttributeValue>();
		room.put(DynDBRooms.RoomColumn, new AttributeValue().withS(roomName));
		GetItemResult fromDb = ddr.readItem(room, true);
		if(fromDb==null){
			sent = 0;
			room.put(DynDBRooms.LastFilteredChunkColumn, new AttributeValue().withN(Integer.toString(sent)));
			ddr.addItem(room);
		}
		else	
			sent = Integer.parseInt(fromDb.getItem().get(DynDBRooms.LastFilteredChunkColumn).getN());
		System.out.println("MainThread: all resumed");
	}
	
	/**
	 * Extract a message from each user data structures and send it to the filter algorithm.
	 * After it has to update all the counters and the databases.
	 */
	public void run(){
		System.out.println("MainThread: starting");
		boolean cont = true;
		started = false;
		oldTimer = System.currentTimeMillis();
		while (cont){
			cont = dataManipulation();
		}
	}
	
	private boolean dataManipulation(){
		/* Retrieve data */
		ArrayList<byte[]> data = getData();
		//System.out.println("MainThread: retrieved data");
		byte[] result = new byte[0];
		/* Something to filter */
		if(!data.isEmpty()){
			started = true;
			try {
				result = CleaningAlgorithm.cleaner(data, roomName);
			} catch (IOException e) {
				e.printStackTrace();
			}
			/*if(data.size()==2)
				result = trimPaddedArray(result);*/
			if(result!=null){
				ByteBuffer bb = ByteBuffer.wrap(result);
				bb.rewind();
				byte[] part = new byte[maxMsgDim];
				while(bb.remaining()>maxMsgDim){
					bb.get(part);
					ByteBuffer partial = ByteBuffer.wrap(part);
					SendMessage.sendMessage(savingQueueUrl, roomName, sent, 0, partial);
					sent++;
				}
				if(bb.remaining()>0){
					byte[] bbarray = new byte[bb.remaining()];
					bb.get(bbarray);
					ByteBuffer bb2 = ByteBuffer.wrap(bbarray);  
					SendMessage.sendMessage(savingQueueUrl, roomName, sent, 0, bb2);
					sent++;
				}
			}
		}
		/* If just started we have to wait for the first messages */
		else if(started){
			ByteBuffer bb = ByteBuffer.allocate(4);
			bb.putInt(-1);
			bb.rewind();
			SendMessage.sendMessage(savingQueueUrl, roomName, sent, 1, bb);
			sent++;
		}
		/* Update database with the sent messages counter */
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		Map<String, AttributeValueUpdate> attributeUpdates = new HashMap<String, AttributeValueUpdate>();
		key.put(DynDBRooms.RoomColumn, new AttributeValue().withS(roomName));
		attributeUpdates.put(DynDBRooms.LastFilteredChunkColumn, new AttributeValueUpdate().withAction("PUT").withValue(new AttributeValue().withN(Integer.toString(sent))));
		ddr.updateItem(key, attributeUpdates);

		/* Update the database with the users data */
		Set<String> users = peeked.keySet();
		for(String u : users){
			UserData ud = perUserData.get(u);
			int num = peeked.get(u);
			int flag = 0;
			for(int i=0; i<num; i++){
				ComparableMessage cmsg = ud.pollOrdered();
				Map<String, MessageAttributeValue> mapMav = cmsg.msg.getMessageAttributes();
				MessageAttributeValue mav = mapMav.get(ChunkTag);
				int chunk = Integer.parseInt(mav.getStringValue());
				ud.setLastFilteredChunk(chunk);
				mav = mapMav.get(FlagsTag);
				
				flag = Integer.parseInt(mav.getStringValue());
				
				/* A normal message */
				if(flag == 0){
					key = new HashMap<String, AttributeValue>();
					attributeUpdates = new HashMap<String, AttributeValueUpdate>();
					key.put(DynDBUsers.RoomColumn, new AttributeValue().withS(roomName));
					key.put(DynDBUsers.UserColumn, new AttributeValue().withS(u));
					attributeUpdates.put(DynDBUsers.LastFilteredChunkColumn, new AttributeValueUpdate().withAction("PUT").withValue(new AttributeValue().withN(Integer.toString(ud.getLastFilteredChunk()))));
					addc.updateItem(key, attributeUpdates);
				}
				/* The user exited or crashed */
				else{
					if(!NoDelete){
						key = new HashMap<String, AttributeValue>();
						key.put(DynDBUsers.RoomColumn, new AttributeValue().withS(roomName));
						key.put(DynDBUsers.UserColumn, new AttributeValue().withS(u));
						addc.removeItem(key);

						sqs.deleteQueue(urls.get(u));
					}
					perUserData.remove(u);
					peeked.remove(u);
					urls.remove(u);
					i=num;
					continue;
				}
				
				if(!NoDelete){
					if(flag != timeoutSignal){
						String receipt = cmsg.msg.getReceiptHandle();
						sqs.deleteMessage(urls.get(u), receipt);
					}
				}
			}
			
		}
		/* If the map is empty and started is true, the thread has to stop */
		if(perUserData.isEmpty() && started){
			st.terminate();
			ht.terminate();
			ByteBuffer bb = ByteBuffer.allocate(4);
			bb.putInt(-1);
			bb.rewind();
			SendMessage.sendMessage(savingQueueUrl, roomName, sent, 1, bb);
			sent++;

			if(!NoDelete){
				deleteDynDBRooms();
			}
			
			try {
				RESTRequest.putStopRecordingRequest(roomName);
				RESTRequest.deleteRoomRequest(roomName);
			} catch (IOException | RESTException e) {
				e.printStackTrace();
			}
			System.out.println("MainThread: registration closed");
			return false;
		}
		else if(!started && (System.currentTimeMillis()-oldTimer)>timeout){
			st.terminate();
			ht.terminate();

			if(!NoDelete){
				deleteDynDBRooms();
			}
			
			try {
				RESTRequest.putStopRecordingRequest(roomName);
				RESTRequest.deleteRoomRequest(roomName);
			} catch (IOException | RESTException e) {
				e.printStackTrace();
			}
			System.out.println("MainThread: no activity during the first 5 minutes. Closing...");
			return false;
		}
		return true;
	}
	
	/*private byte[] trimPaddedArray(byte[] result) {
		int i=0, j=result.length-1;
		while(i<result.length && result[i]==0){
			i++;
		}
		if(i==result.length)
			return null;
		while(j>=0 && result[j]==0){
			j--;
		}
		return Arrays.copyOfRange(result, i, j+1);
	}*/

	public static void deleteDynDBRooms(){
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put(DynDBRooms.RoomColumn, new AttributeValue().withS(roomName));
		ddr.removeItem(key);
	}
	
	/**
	 * Get data from the queues of the user. The data is valid only if not far more than one second
	 * from the minor timestamp.
	 * @return An ArrayList containing byte arrays of data from every users. It could be empty
	 */
	public static ArrayList<byte[]> getData(){
		//System.out.println("MainThread: retrieving data");
		ArrayList<byte[]> toClean = new ArrayList<byte[]>();
		Set<String> users = perUserData.keySet();
		/* Get the minimum timestamp from all the users. This function blocks waiting from messages
		 * from everyone
		 */
		long timeStamp = getMinimumTimestamp();
		//System.out.println("MainThread: Retrieved minimum timestamp: "+timeStamp);
		for(String u : users){
			UserData ud = perUserData.get(u);
			ArrayList<byte[]> byteArrays = new ArrayList<byte[]>();
			peeked.put(u, 0);
			/* Peek (not poll) a number of messages equal to msgToClean for every user */
			for(int j=0; j<msgToClean; j++){
				/* TODO If flag 2 -> don't look to bb.capacity
				 * If flag 1 -> ...
				 * Change timestamp check: read timestamp from custom attribute Timestamp;
				 * Compute threshold from ByteBuffer dimension: dim/88200
				 * 
				 */
				
				ComparableMessage msg = null;
				/* Waiting for messages. We have waited also for the minimumTimestamp, so this wait is minimal */
				try {
					msg = ud.peekIndexedOrdered(j);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Map<String, MessageAttributeValue> msgAttr = msg.msg.getMessageAttributes();
				
				/* Retrieve the data. If the flag is 2, it's a fake messages */
				ByteBuffer bb = msgAttr.get(DataTag).getBinaryValue();
				int flag = Integer.parseInt(msgAttr.get(FlagsTag).getStringValue());
				if(flag==2){
					peeked.put(u, j+1);
					j=msgToClean;
					continue;
				}
				
				if(timestampSecondThresh==0){
					timestampSecondThresh = (int)(bb.capacity()/88.2);
				}
				
				long msgTimestamp = Long.parseLong(msgAttr.get(TimeTag).getStringValue());
				
				/* Message greater than one second: keep it for further elaborations */
				if(msgTimestamp - timeStamp >= timestampSecondThresh*(j+1)){
					System.err.println("MainThread: ATTENTION, Future chunk");
					j=msgToClean;
					continue;
				}
				/* An old message: to be removed */
				//else if(msgTimestamp - timeStamp < -timestampSecondThresh*(j+1)){
				else if(msgTimestamp - timeStamp < 0){
					System.err.println("MainThread: ATTENTION, Old chunk");
					msg = ud.pollOrdered();
					j--;
					continue;
				}
				
				/* Good data, to be filtered */
				byteArrays.add(bb.array());
				peeked.put(u, j+1);
				if(flag==1)
					j=msgToClean;
				
			}
			if(!byteArrays.isEmpty())
				toClean.add(concatenateArrays(byteArrays));
		}
		return toClean;
	}
	
	/**
	 * Concatenate multiple arrays of bytes
	 * @param Arrays
	 * @return An unique byte array
	 */
	private static byte[] concatenateArrays(ArrayList<byte[]> arrays){
		byte[] last = new byte[0];
		for(byte[] i : arrays){
			byte[] tmp = last;
			last = new byte[last.length+i.length];
			System.arraycopy(tmp, 0, last, 0, tmp.length);
			System.arraycopy(i, 0, last, tmp.length, i.length);
		}
		return last;
	}
	
	/**
	 * Compute the minimum timestamp for all the users
	 * @return
	 */
	private static long getMinimumTimestamp(){
		Set<String> users = perUserData.keySet();
		Long timestamp = Long.MAX_VALUE;
		for(String u : users){
			UserData ud = perUserData.get(u);
			ComparableMessage cmsg;
			/* Waits for a message from every users */
			polling:while(true){
				cmsg = ud.peekOrdered();
				if(cmsg!=null) break polling;
			}
			Map<String, MessageAttributeValue> attrs = cmsg.msg.getMessageAttributes();
			long msgTimestamp = Long.parseLong(attrs.get(TimeTag).getStringValue());
			if(msgTimestamp<timestamp && msgTimestamp > oldTimestamp){
				timestamp = msgTimestamp;
			}
		}
		if(timestamp<Long.MAX_VALUE)
			oldTimestamp = timestamp;
		return timestamp;
	}
	
	/**
	 * Add a message in the structures of a specific user
	 * @param User identification
	 * @param Message
	 * @return True if it's a normal message. False if a conclusive message
	 */
	public static boolean addData(String user, Message data){
		UserData ud = perUserData.get(user);
		ComparableMessage msg = new ComparableMessage(data);
		Map<String, MessageAttributeValue> attrs = data.getMessageAttributes();
		int actualChunkId = Integer.parseInt(attrs.get(ChunkTag).getStringValue());
		if(actualChunkId==ud.getLastOrderedChunk()+1){
			ud.offerOrdered(msg);
			ud.setLastOrderedChunk(actualChunkId);
			
			Map<String,MessageAttributeValue> attr = msg.msg.getMessageAttributes();
			if(Integer.parseInt(attr.get(FlagsTag).getStringValue())>0){
				return false;
			}
		}
		else if(actualChunkId <= ud.getLastOrderedChunk()){
			return true;			
		}
		else{
			if(ud.getFirstOutOfOrderChunk()>actualChunkId)
				ud.setFirstOutOfOrderChunk(actualChunkId);
			ud.offerOutOfOrder(msg);
		}
		
		boolean stop = false;
		reorderCycle:while(!stop){
			if(ud.getFirstOutOfOrderChunk()==ud.getLastOrderedChunk()+1){
				ComparableMessage outMsg = ud.pollOutOfOrder();
				if(outMsg==null){
					stop = true;
					ud.setFirstOutOfOrderChunk(Integer.MAX_VALUE);
					break reorderCycle;
				}
				ud.offerOrdered(outMsg);
				
				Map<String,MessageAttributeValue> outAttr = outMsg.msg.getMessageAttributes();
				if(Integer.parseInt(outAttr.get(FlagsTag).getStringValue())>0){
					return false;
				}
				
				ud.setLastOrderedChunk(ud.getFirstOutOfOrderChunk());
				outMsg = ud.peekOutOfOrder();
				if(outMsg==null){
					stop = true;
					ud.setFirstOutOfOrderChunk(Integer.MAX_VALUE);
					break reorderCycle;
				}
				outAttr = outMsg.msg.getMessageAttributes();
				ud.setFirstOutOfOrderChunk(Integer.parseInt(outAttr.get(ChunkTag).getStringValue()));
			}
			else if(ud.getFirstOutOfOrderChunk() <= ud.getLastOrderedChunk()){
				ud.pollOutOfOrder();
				ComparableMessage outMsg = ud.peekOutOfOrder();
				if(outMsg==null){
					stop = true;
					ud.setFirstOutOfOrderChunk(Integer.MAX_VALUE);
					break reorderCycle;
				}
				Map<String,MessageAttributeValue> outAttr = outMsg.msg.getMessageAttributes();
				ud.setFirstOutOfOrderChunk(Integer.parseInt(outAttr.get(ChunkTag).getStringValue()));
			}
			else
				stop = true;
		}
		return true;
	}
	
	public static void main(String[] args){
		File dir = new File(System.getProperty("user.home")+"/.sdcc");
		if(!dir.exists())
			dir.mkdir();
		File sett = new File(propertiesFile);
		if(!sett.exists()){
			try {
				sett.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		readProperties();		
		
		if(args.length!=3){
			System.err.println("Usage: command <Room name> <Service queue url> <Saving queue>");
			System.exit(1);
		}
		
		MainThread mt = new MainThread(args);
		mt.start();
	}
	
	public static String correctUrl(String wrongUrl){
		String queueName = wrongUrl.substring(wrongUrl.lastIndexOf("/")+1);
		String url = "";
		try{
			url = sqs.getQueueUrl(queueName).getQueueUrl();
		}
		catch(Exception e){
			if(MainThread.ht!=null){
				ht.terminate();
				if(ht.isAlive()){
					try {
						ht.join();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			if(MainThread.st!=null){
				st.terminate();
				if(st.isAlive()){
					try {
						st.join();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			System.exit(1);
		}
		return url;
	}
	
	private static void readProperties() {

		settings = new Properties();
		try {
			settings.load(new FileInputStream(propertiesFile ));
		} catch (IOException e) {
			System.out.println("Unable to load settings file. Using default parameters.");

			settings.put("Token", "");
			settings.put("Region", "");
			settings.put("Debug", "false");
			settings.put("Timeout", "300000");
			settings.put("MsgNum", "1");
			settings.put("ServiceWait", "1");
			settings.put("HoundWait", "7000");
			settings.put("UrlREST", "http://rest.codep.quantumb.it/rest/rooms/");
		}

		/* Reload token */
		try {
			String token = (String) settings.get("Token");
			RESTRequest.setToken(token);

			if (!token.matches("^[a-zA-Z0-9.]+$")) {
				System.out.println("Invalid token, restoring default value.");
				token = "";
				settings.put("Token", token);
			}

		} catch (NullPointerException e) {
			String token = "";
			settings.put("Token", token);
		}

		try {
			String regionName = (String) settings.get("Region");
			if(regionName.equals(""))
				Regs = null;
			else 
				Regs = Regions.fromName(regionName);

			if (Regs == null) {
				System.out.println("Unknown or null region, set current value.");
			}

		} catch (NullPointerException e) {
			String regionName = "eu-central-1";
			settings.put("Region", regionName);
		}

		try {
			NoDelete = Boolean.parseBoolean((String) settings.get("Debug"));
		} catch (NullPointerException e) {
			NoDelete = false;
			settings.put("Debug", "false");
		}
		
		try {
			String timeoutS = (String) settings.get("Timeout");
			if(timeoutS!=null)
				timeout = Long.parseLong(timeoutS);
			else
				throw new NullPointerException("Timeout null");
		} catch (NullPointerException e) {
			timeout = 300000;
			settings.put("Timeout", "300000");
		}
		
		try {
			String msgS = (String) settings.get("MsgNum");
			if(msgS != null)
				msgToClean = Integer.parseInt(msgS);
			else
				throw new NullPointerException("Msg null");
		} catch (NullPointerException e) {
			msgToClean = 1;
			settings.put("MsgNum", "1");
		}
		
		try {
			String secS = (String) settings.get("ServiceWait");
			if(secS!=null)
				ServiceThread.WaitSeconds = Integer.parseInt(secS);
			else
				throw new NullPointerException("SecS null");
		} catch (NullPointerException e) {
			ServiceThread.WaitSeconds = 1;
			settings.put("ServiceWait", "1");
		}
		
		try {
			String timeS = (String) settings.get("HoundWait");
			if(timeS!=null)
				HoundThread.WaitTime = Integer.parseInt(timeS);
			else
				throw new NullPointerException("TimeS null");
		} catch (NullPointerException e) {
			HoundThread.WaitTime = 7000;
			settings.put("HoundWait", "7000");
		}
		
		try{
			RESTRequest.RESTurl = (String) settings.get("UrlREST");
			if(RESTRequest.RESTurl==null)
				throw new NullPointerException();
		}
		catch (NullPointerException e){
			RESTRequest.RESTurl = "http://rest.codep.quantumb.it/rest/rooms/";
			settings.put("UrlREST", RESTRequest.RESTurl);
		}

		try {
			settings.store(new FileOutputStream(propertiesFile), "Filter server properties");
		} catch (IOException e1) {
			System.out.println("Unable to store default settings. Going on anyway.");
		}

	}
}
