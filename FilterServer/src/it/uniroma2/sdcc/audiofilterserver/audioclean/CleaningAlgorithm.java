package it.uniroma2.sdcc.audiofilterserver.audioclean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.musicg.wave.Wave;
import com.musicg.wave.WaveFileManager;
import com.musicg.wave.WaveHeader;

/**
 * Class containing the first function to call for the cleaning
 * @author Daniele De Angelis
 *
 */
public class CleaningAlgorithm{
	static float INF=Float.MAX_VALUE;
	public static boolean WINDOW=true;
	public static String fileName;				//name for the saves
	public static short[] amplitude;		//amplitudes in short values
	public static ArrayList<float[]> normalizedAmplitudes; //amplitude in float values
	public static ArrayList<float[]> amplitudeReady;		 //amplitude in float values with same lenght
	static float[][] hToPlotW;		//matrix with the h value (windows mode)
	static final int offsetXCorr=10;
	static int SAMPLE_RATE=44100;
	private static WaveHeader wh;
	
	public static final Logger Log = Logger.getLogger(CleaningAlgorithm.class.getName());
	
	/**
	 * First part of the cleaning algorithm. It does the preparation and the synchronization
	 * @param ct
	 * 		Caller thread
	 * @param data
	 * 		Data
	 * @param name
	 * 		File name
	 * @return
	 * 		An array with the final data
	 * @throws IOException
	 */
	public static byte[] cleaner(ArrayList<byte[]> data, String name) throws IOException{
		Log.setLevel(Level.INFO);
		
//		fileName=name;
//		Wave w=new Wave(fileName);
		wh= new WaveHeader();
		wh.setSampleRate(44100);
		SAMPLE_RATE=wh.getSampleRate();
		int[] index=new int[2];
		normalizedAmplitudes=new ArrayList<float[]>();
		amplitudeReady=new ArrayList<float[]>();
		int offset;
		
		// Input verification
		if(data.size()<=2){
			//If we have only one array of byte then we have only the data from the recorder
			amplitude=getShortsArray(data.get(0));
			WaveManipulation.getNormalizedAmplitudes(wh.getBitsPerSample());
			return WaveManipulation.fromFloatsToByte(AlgorithmWindows.levelAudio(normalizedAmplitudes.get(0)));
		}				
		//ct.forNorm=true; TODO forse utile in fase più avanzata della programmazione
		offset=offsetXCorr;		// Reading cross correlation offset 
		Syncing.print=false;
		Log.info("Cleaning: number of tracks: "+data.size());
		// creating the arrays to contain the tracks 
		for(int i=0;i<data.size();i++){
			amplitude=getShortsArray(data.get(i));
			WaveManipulation.getNormalizedAmplitudes(wh.getBitsPerSample());
		}
		//Preration and synchronization phase
		normalizedAmplitudes.trimToSize();
		Syncing.zeroPadding();
		index=Syncing.selectionSyncronization(offset);
		System.out.println("Max Correlation = "+Syncing.maxCorr);
		//Calling the function that do the next phases
		//return AlgorithmWindows.algorithm(index[0],index[1]);
		return WaveManipulation.fromFloatsToByte(AlgorithmWindows.algorithm(index[0],index[1]));
	}
	
	public static void main(String[] args){
		ArrayList<byte[]> data = openFiles(args);
		byte[] last = new byte[0];
		try {
			last = cleaner(data, "Ciao");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WaveFileManager wfm = new WaveFileManager();
		Wave w = new Wave(wh, last);
		wfm.setWave(w);
		wfm.saveWaveAsFile("result.wav");
	}
	
	private static ArrayList<byte[]> openFiles(String[] args) {
		Wave w = new Wave();
		ArrayList<byte[]> data = new ArrayList<byte[]>();
		for(String i : args){
			w = new Wave(i);
			data.add(w.getBytes());
		}
		return data;
	}

	/**
	 * Converts an array of bytes into an array of shorts
	 * @param data
	 * 		Array of bytes
	 * @return
	 * 		Conversion of the bytes into shorts
	 */
	public static short[] getShortsArray(byte[] data){
		int bytePerSample = wh.getBitsPerSample() / 8;
		int numSamples = data.length / bytePerSample;
		short[] amplitudes = new short[numSamples];
		
		int pointer = 0;
		for (int i = 0; i < numSamples; i++) {
			short amplitude = 0;
			for (int byteNumber = 0; byteNumber < bytePerSample; byteNumber++)
				// little endian
				amplitude |= (short) ((data[pointer++] & 0xFF) << (byteNumber * 8));
			amplitudes[i] = amplitude;
		}
		
		return amplitudes;
	}
}