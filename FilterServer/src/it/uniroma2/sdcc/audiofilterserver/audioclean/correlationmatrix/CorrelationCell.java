package it.uniroma2.sdcc.audiofilterserver.audioclean.correlationmatrix;

/**
 * Object cell for correlation matrix
 * @author Daniele De Angelis
 *
 */
public class CorrelationCell{
	public double correlation;
	public int delay;
	/**
	 * Constructor
	 */
	public CorrelationCell(){}
	/**
	 * Constructor
	 * @param maxcorr
	 * 		correlation value
	 * @param offset
	 * 		offset for the shifting
	 */
	public CorrelationCell(double maxcorr, int offset){
		correlation=maxcorr;
		delay=offset;
	}
	
	public void setCorrelation(double corr){
		correlation = corr;
	}
	
	public void setDelay(int del){
		delay = del;
	}
}
