#!/bin/bash

mkdir ./output

FLOOR=10

NOISEFILES=($1/*)
SOUNDFILES=($2/*)

for ((i=0;i<=${#SOUNDFILES[@]};i++)); do
	
	SOUNDNAME=$(basename "${SOUNDFILES[$i]}")
	EXT="${SOUNDNAME##*.}"
	SOUNDNAME="${SOUNDNAME%.*}"
	
	LENGTH=$(soxi -D ${SOUNDFILES[$i]})
	ILENGTH=${LENGTH%.*}
	
	if [ -n "${SOUNDFILES[$i]}" ]; then
		for((j=0;j<=${#NOISEFILES[@]};j++)); do			
			NOISENAME=$(basename "${NOISEFILES[$j]}")
			EXT="${NOISENAME##*.}"
			NOISENAME="${NOISENAME%.*}"
			
			NOISEL=$(soxi -D ${NOISEFILES[$j]})
			INOISEL=${NOISEL%.*}
			
			RANGE=($ILENGTH/2)-INOISEL
			
			number=0
			while [ "$number" -le $FLOOR ]
			do
				number=$RANDOM
				let "number %= $RANGE"
			done
			
			if [ -n "${NOISEFILES[$j]}" ]; then
				echo "Noise file: ${NOISEFILES[$j]}"
				echo "Sound file: ${SOUNDFILES[$i]}"
				echo "Freq: $number"
				python sox.py "${NOISEFILES[$j]}" "${SOUNDFILES[$i]}" "$number" "./output/$NOISENAME-$SOUNDNAME-$number.mp3"
				ffmpeg -i "./output/$NOISENAME-$SOUNDNAME-$number.mp3" -c:a libfdk_aac "./output/$NOISENAME-$SOUNDNAME-$number.aac"
				#ffmpeg -i "./output/$NOISENAME-$SOUNDNAME-$number.mp3" -strict -2 -f adts -c aac -ac 1 -b:a 32000 "./output/$NOISENAME-$SOUNDNAME-$number.aac"
			fi
		done
	fi
done

rm ./output/*.mp3
