#!/bin/bash

NOISEFILES=($1/*)

for ((i=0;i<=${#NOISEFILES[@]};i++)); do
	
	NOISENAME=$(basename "${NOISEFILES[$i]}")
	EXT="${NOISENAME##*.}"
	NOISENAME="${NOISENAME%.*}"
	
	if [ -n "${NOISEFILES[$i]}" ]; then
		sox NOISEFILES[$i] -c 1 "$1/$NOISENAME.$EXT" avg -l
	fi
done

