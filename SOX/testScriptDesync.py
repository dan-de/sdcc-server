#!/usr/bin/python
import os
import sys
import csv
import time
import shutil

seconds = 10

if len(sys.argv)!=6:
	print "Usage: python scriptTest.py <SDCC_TCP_SERVER> <Files directory> <Executable> <RoomID> <CSV path>;"
	exit(1)
	
#os.system("export SDCC_TCP_SERVER="+sys.argv[1])

filedir=sys.argv[2]
executable=sys.argv[3]
roomid=sys.argv[4]
csvfile=sys.argv[5]

if not filedir.endswith('/'):
	filedir=filedir+"/"
	
csvopen = open(csvfile, 'rb')

files = os.listdir(filedir)

newdir = "./trimmed/"
if not os.path.exists(newdir):
	os.makedirs(newdir)

index = 0
for i in files:
	if index != 0:
		print index
		newfile = (newdir+str(seconds*index)+"-")+i[:-3]+"mp3"
		os.system("ffmpeg -i "+filedir+i+" "+(newdir+i)[:-3]+"mp3")
		os.system("sox "+(newdir+i)[:-3]+"mp3"+" "+newfile+" trim "+str(seconds*index))
		os.system("ffmpeg -i "+newfile+" -c:a libfdk_aac "+newfile[:-3]+"aac")
		os.remove(newfile)
		os.remove(newdir+i[:-3]+"mp3")
		print "File number "+str(index)+", "+newfile[:-3]+"aac"				
		index = index +1
	else:
		print "First file, "+i
		os.system("cp "+filedir+i+" "+newdir+str(index)+"-"+i)
		index = index +1
		
files = os.listdir(newdir)
files.sort()
print files

try:
	reader = csv.reader(csvopen,delimiter=',',quotechar='\n')
	index = 0;
	millis = int(round(time.time() * 1000))
	print millis
	for i in files:
		user = reader.next()
		print str(user)
		print user==None 
		if user==None:
			break
		print i+", "+str(user)
		while int(round(time.time() * 1000)) < millis+(seconds*index*1000):
			continue
		print "File number "+str(index)+", millis="+str(millis+(seconds*index*1000))				
		os.system("export SDCC_TCP_SERVER="+sys.argv[1]+" && java -jar "+executable+" -i "+newdir+i+" -r "+roomid+" -u "+str(user[0])+" -t "+str(user[1])+" -b 96 &")
		index = index +1
except Exception as e:
	print "I/O error({0}): {1}".format(e.errno, e.strerror)
finally:
	csvopen.close()
	while os.system("pgrep java") == 0:
		pass
	print "Removing "+newdir 
	shutil.rmtree(newdir)
	exit()
