#!/usr/bin/python
import sys
import os
import subprocess

if len(sys.argv)!=5:
	print "Usage: python sox.py <Noise file path> <Sound file path> <Offset> <Output file path>;"
	exit(1)
	
noise=sys.argv[1]
sound=sys.argv[2]
offset=sys.argv[3]
output=sys.argv[4]

result=subprocess.check_output(["soxi","-D",sound])
iresult=int(float(result))

times=iresult/int(float(offset))

command="sox -m "+sound
#commplusopt=command.split(" ")

for i in range(1,times+1):
	command=command+" \"|sox "+noise+" -p pad "+str(i*int(float(offset)))+"\""
	#commplusopt.append("\"|sox "+noise+" -p pad "+str(i*int(float(offset)))+"\"")

command=command+" "+output
#commplusopt.append(output)

print str(command)
#subprocess.call(commplusopt)
os.system(command)
