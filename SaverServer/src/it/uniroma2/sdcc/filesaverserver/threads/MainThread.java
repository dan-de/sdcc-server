package it.uniroma2.sdcc.filesaverserver.threads;

import it.sauronsoftware.jave.EncoderException;
import it.uniroma2.sdcc.filesaverserver.utils.AudioConverter;
import it.uniroma2.sdcc.filesaverserver.utils.ComparableMessage;
import it.uniroma2.sdcc.filesaverserver.utils.DynDBServers;
import it.uniroma2.sdcc.filesaverserver.utils.RESTException;
import it.uniroma2.sdcc.filesaverserver.utils.RESTRequest;
import it.uniroma2.sdcc.filesaverserver.utils.S3Manager;
import it.uniroma2.sdcc.filesaverserver.utils.UserData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.musicg.wave.Wave;
import com.musicg.wave.WaveFileManager;
import com.musicg.wave.WaveHeader;

public class MainThread extends Thread{
	
	public static AWSCredentials credentials;

	public static final String TableServers = "CodeP_Servers";
	
	/* Settings file */
	public static String CDNUrl;
	public static Regions Regs;
	public static boolean NoDelete	= false;
	/*
	 * RESTRequest.token
	 * HoundThread.WaitSeconds
	 * Orderer.WaitTime
	 * Orderer.timeout
	 */
	
	public static final String FileTag 	= "FileName";
	public static final String ChunkTag = "ChunkId";
	public static final String FlagsTag = "Flags";
	public static final String DataTag 	= "Data";
	private static final int stopSignal = 1;
	
	protected static AmazonSQS sqs;
	protected static DynDBServers dds;
	
	private String fileWav;
	private String fileCompr;
	
	protected static UserData data;

	private static Properties settings;

	private static final String propertiesFile = System.getProperty("user.home")+"/.sdcc/settingsSaver.txt";;
	
	protected static String roomName;
	public static String queueUrl;
	
	protected static HoundThread ht;
	private OrdererThread ot;
	
	public MainThread(String[] args){
		
		credentials = null;
		try {
			credentials = new ProfileCredentialsProvider("default").getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException(
					"Cannot load the credentials from the credential profiles file. " +
							"Please make sure that your credentials file is at the correct " +
							"location (/home/daniele/.aws/credentials), and is in valid format.",
							e);
		}
		
		try{
			sqs = new AmazonSQSClient(credentials);
			if(Regs!=null)
				sqs.setRegion(Region.getRegion(Regs));
			else
				sqs.setRegion(Regions.getCurrentRegion());
			dds = new DynDBServers("CodeP_Servers");
		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which means your request made it " +
					"to Amazon SQS, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered " +
					"a serious internal problem while trying to communicate with SQS, such as not " +
					"being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
		roomName = args[0];
		queueUrl = correctUrl(args[1]);
		
		ht = new HoundThread(roomName, queueUrl);
		try{
			ht.setPriority(ht.getPriority()+1);
		}
		catch(Exception e){
			System.err.println("MainThread: Impossible to set a new priority");
		}
		ht.start();
		
		data =  new UserData();
		
		fileWav = roomName.concat(".wav");
		fileCompr = roomName.concat(".mp4");
		
		ot =  new OrdererThread(queueUrl);
		ot.start();
	}
	
	public void run(){
		System.out.println("MainThread: starting");
		WaveHeader wh = new WaveHeader();
		wh.setSampleRate(44100);
		Wave w =  new Wave();
		WaveFileManager wfm = new WaveFileManager();
		ArrayList<byte[]> byteData = new ArrayList<byte[]>();
		boolean cont = true;
		while(cont){
			ComparableMessage cmsg = null;
			try{	
				cmsg = data.takeOrdered();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("MainThread: retrieved message");
			Map<String, MessageAttributeValue> mattr = cmsg.msg.getMessageAttributes();
			int flag = Integer.parseInt(mattr.get(FlagsTag).getStringValue()); 
			if(flag==stopSignal){
				cont = false;
			}
			else{
				ByteBuffer bb = mattr.get(DataTag).getBinaryValue();
				byte[] barray = bb.array();
				byteData.add(barray);
			}
		}
		
		System.out.println("MainThread: all messages retrieved. Starting to save");
		byte[] complete = concatenateArrays(byteData);
		w = new Wave(wh, complete);
		wfm.setWave(w);
		wfm.saveWaveAsFile(fileWav);
		
		try {
			AudioConverter.encoding(fileWav, fileCompr);
		} catch (EncoderException | IOException e) {
			e.printStackTrace();
		}
		
		Map<String, Long> infos = null;
		try {
			infos = AudioConverter.getFileInfo(fileCompr);
		} catch (EncoderException e) {
			e.printStackTrace();
			return;
		}
		
		System.out.println("MainThread: uploading file to S3");
		try {
			S3Manager.sendFileToS3(roomName+".mp4", fileCompr);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String fileUrl = CDNUrl+"/"+fileCompr.substring(fileCompr.lastIndexOf("/")+1);
		String time = formatTime(infos.get(AudioConverter.LengthInfo));
		Long size = infos.get(AudioConverter.SizeInfo);
		try {
			RESTRequest.sendInformations(roomName, fileUrl, size, time);
		} catch (IOException | RESTException e) {
			e.printStackTrace();
		}
		
		if(!NoDelete)
			sqs.deleteQueue(queueUrl);
		ht.terminate();
		File f = new File(fileWav);
		f.delete();
		f = new File(fileCompr);
		f.delete();
		System.out.println("MainThread: all works done. Closing...");
	}
	
	private static void readProperties() {
	
		settings = new Properties();
		try {
			settings.load(new FileInputStream(propertiesFile  ));
		} catch (IOException e) {
			System.out.println("Unable to load settings file. Using default parameters.");
	
			settings.put("Token", "");
			settings.put("Region", "");
			settings.put("Debug", "false");
			settings.put("HoundWait", "7000");
			settings.put("OrderWait", "1");
			settings.put("OrderTimeout", "300000");
			settings.put("UrlCDN", "http://cdn.codep.quantumb.it");
			settings.put("UrlREST", "http://rest.codep.quantumb.it/rest/recordings/");
		}
	
		/* Reload token */
		try {
			String token = (String) settings.get("Token");
			RESTRequest.setToken(token);
	
			if (!token.matches("^[a-zA-Z0-9.]+$")) {
				System.out.println("Invalid token, restoring default value.");
				token = "";
				settings.put("Token", token);
			}
	
		} catch (NullPointerException e) {
			String token = "";
			settings.put("Token", token);
		}
		
		try {
			CDNUrl = (String) settings.get("UrlCDN");
	
			if (!CDNUrl.matches("^[a-zA-Z0-9.]+$")) {
				System.out.println("Invalid token, restoring default value.");
				CDNUrl = "http://cdn.codep.quantumb.it";
				settings.put("UrlCDN", CDNUrl);
			}
	
		} catch (NullPointerException e) {
			CDNUrl = "http://cdn.codep.quantumb.it";
			settings.put("UrlCDN", CDNUrl);
		}
	
		try {
			String regionName = (String) settings.get("Region");
			if(regionName.equals(""))
				Regs = null;
			else 
				Regs = Regions.fromName(regionName);
	
			if (Regs == null) {
				System.out.println("Unknown or null region, set current value.");
			}
	
		} catch (NullPointerException e) {
			String regionName = "eu-central-1";
			settings.put("Region", regionName);
		}
	
		try {
			NoDelete = Boolean.parseBoolean((String) settings.get("Debug"));
		} catch (NullPointerException e) {
			NoDelete = false;
			settings.put("Debug", "false");
		}
		
		try {
			String timeS = (String) settings.get("HoundWait");
			if(timeS!=null)
				HoundThread.WaitTime = Integer.parseInt(timeS);
			else
				throw new NullPointerException("TimeS null");
		} catch (NullPointerException e) {
			HoundThread.WaitTime = 7000;
			settings.put("HoundWait", "7000");
		}
		
		try {
			String timeS = (String) settings.get("OrderWait");
			if(timeS!=null)
				OrdererThread.WaitSeconds = Integer.parseInt(timeS);
			else
				throw new NullPointerException("TimeS null");
		} catch (NullPointerException e) {
			OrdererThread.WaitSeconds = 1;
			settings.put("OrderWait", "1");
		}
		
		try {
			String timeS = (String) settings.get("OrderTimeout");
			if(timeS!=null)
				OrdererThread.timeout = Integer.parseInt(timeS);
			else
				throw new NullPointerException("TimeS null");
		} catch (NullPointerException e) {
			OrdererThread.timeout = 300000;
			settings.put("OrderTimeout", "300000");
		}
		
		try{
			RESTRequest.RESTurl = (String) settings.get("UrlREST");
			if(RESTRequest.RESTurl==null){
				throw new NullPointerException();
			}
		}
		catch (NullPointerException e){
			RESTRequest.RESTurl = "http://rest.codep.quantumb.it/rest/recordings/";
			settings.put("UrlREST", RESTRequest.RESTurl);
		}
	
		try {
			settings.store(new FileOutputStream(propertiesFile), "Filter server properties");
		} catch (IOException e1) {
			System.out.println("Unable to store default settings. Going on anyway.");
		}
	
	}

	private static byte[] concatenateArrays(ArrayList<byte[]> arrays){
		byte[] last = new byte[0];
		for(byte[] i : arrays){
			byte[] tmp = last;
			last = new byte[last.length+i.length];
			System.arraycopy(tmp, 0, last, 0, tmp.length);
			System.arraycopy(i, 0, last, tmp.length, i.length);
		}
		return last;
	}
	
	public static void main (String[] args){
		File dir = new File(System.getProperty("user.home")+"/.sdcc");
		if(!dir.exists())
			dir.mkdir();
		File sett = new File(propertiesFile);
		if(!sett.exists()){
			try {
				sett.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		readProperties();
		
		if(args.length!=2){
			System.err.println("Usage: java -jar program.jar <RoomName> <SavingQueueUrl>;");
			return;
		}
		
		MainThread mt = new MainThread(args);
		mt.start();
	}

	public static boolean addData(Message data){
		ComparableMessage msg = new ComparableMessage(data);
		Map<String, MessageAttributeValue> attrs = data.getMessageAttributes();
		int actualChunkId = Integer.parseInt(attrs.get(ChunkTag).getStringValue());
		if(actualChunkId==MainThread.data.getLastOrderedChunk()+1){
			MainThread.data.offerOrdered(msg);
			MainThread.data.setLastOrderedChunk(actualChunkId);
			
			Map<String,MessageAttributeValue> attr = msg.msg.getMessageAttributes();
			if(Integer.parseInt(attr.get(FlagsTag).getStringValue())==stopSignal ){
				return false;
			}
		}
		else if(actualChunkId <= MainThread.data.getLastOrderedChunk()){
			return true;			
		}
		else{
			if(MainThread.data.getFirstOutOfOrderChunk()>actualChunkId)
				MainThread.data.setFirstOutOfOrderChunk(actualChunkId);
			MainThread.data.offerOutOfOrder(msg);
		}
		//System.out.println("Deleting a message.\n");
		
		boolean stop = false;
		reorderCycle:while(!stop){
			if(MainThread.data.getFirstOutOfOrderChunk()==MainThread.data.getLastOrderedChunk()+1){
				ComparableMessage outMsg = MainThread.data.pollOutOfOrder();
				if(outMsg==null){
					stop = true;
					MainThread.data.setFirstOutOfOrderChunk(Integer.MAX_VALUE);
					break reorderCycle;
				}
				MainThread.data.offerOrdered(outMsg);
				
				Map<String,MessageAttributeValue> outAttr = outMsg.msg.getMessageAttributes();
				if(Integer.parseInt(outAttr.get(FlagsTag).getStringValue())==stopSignal){
					return false;
				}
				
				MainThread.data.setLastOrderedChunk(MainThread.data.getFirstOutOfOrderChunk());
				outMsg = MainThread.data.peekOutOfOrder();
				if(outMsg==null){
					stop = true;
					MainThread.data.setFirstOutOfOrderChunk(Integer.MAX_VALUE);
					break reorderCycle;
				}
				outAttr = outMsg.msg.getMessageAttributes();
				MainThread.data.setFirstOutOfOrderChunk(Integer.parseInt(outAttr.get(ChunkTag).getStringValue()));
			}
			else if(MainThread.data.getFirstOutOfOrderChunk() <= MainThread.data.getLastOrderedChunk()){
				MainThread.data.pollOutOfOrder();
				ComparableMessage outMsg = MainThread.data.peekOutOfOrder();
				if(outMsg==null){
					stop = true;
					MainThread.data.setFirstOutOfOrderChunk(Integer.MAX_VALUE);
					break reorderCycle;
				}
				Map<String,MessageAttributeValue> outAttr = outMsg.msg.getMessageAttributes();
				MainThread.data.setFirstOutOfOrderChunk(Integer.parseInt(outAttr.get(ChunkTag).getStringValue()));
			}
			else
				stop = true;
		}
		return true;
	}
	
	public static String correctUrl(String wrongUrl){
		String queueName = wrongUrl.substring(wrongUrl.lastIndexOf("/")+1);
		String url = "";
		try{
			url = sqs.getQueueUrl(queueName).getQueueUrl();
		}
		catch(Exception e){
			if(MainThread.ht!=null){
				ht.terminate();
				if(ht.isAlive()){
					try {
						ht.join();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			System.exit(1);
		}
		return url;
	}
	
	private static String formatTime(Long millis){
		double timem = millis/60000.0;
		String formatDigit = new DecimalFormat("#0.00").format(timem);
		formatDigit = formatDigit.replaceFirst(",", ".");
		return formatDigit;
	}

}
