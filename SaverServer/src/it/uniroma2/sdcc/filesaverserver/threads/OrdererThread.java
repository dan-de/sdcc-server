package it.uniroma2.sdcc.filesaverserver.threads;
import java.util.List;

import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;


public class OrdererThread extends Thread{
	
	public String queueUrl;
	public static int WaitSeconds = 1;
	private static boolean check;
	private static long oldTimer;
	public static long timeout = 300000;
	
	public OrdererThread(String url){
		queueUrl = url;
	}
	
	public void run(){
		readingOrdered();
	}
	
	private void readingOrdered(){
		check = false;
		oldTimer = System.currentTimeMillis();
		System.out.println("OrdererThread: starting");
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl).withAttributeNames("All").withMessageAttributeNames(new String[]{MainThread.FileTag, MainThread.ChunkTag, MainThread.FlagsTag, MainThread.DataTag});
		receiveMessageRequest.setMaxNumberOfMessages(10);
		receiveMessageRequest.setWaitTimeSeconds(WaitSeconds);
		boolean cont = true;
		while(cont){
			List<Message> messages = MainThread.sqs.receiveMessage(receiveMessageRequest).getMessages();
			if(messages.isEmpty() && !check && (System.currentTimeMillis()-oldTimer)>timeout){
				System.err.println("OrderedThread: no message from Filter in 5 minutes. Exiting...");
				MainThread.ht.terminate();
				MainThread.sqs.deleteQueue(MainThread.queueUrl);
				try {
					MainThread.ht.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				finally{
					System.exit(1);
				}
			}
			for(Message i : messages){
				check = true;
				//System.out.println("OrdererThread: new message");
				cont = MainThread.addData(i);
				//System.out.println("OrdererThread: message ordered");
			}
		}
	}

}
