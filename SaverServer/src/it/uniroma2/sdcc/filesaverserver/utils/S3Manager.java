package it.uniroma2.sdcc.filesaverserver.utils;
import it.uniroma2.sdcc.filesaverserver.threads.MainThread;

import java.io.File;
import java.io.IOException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class S3Manager {
	
	public static final String BucketName =  "codep-storage";
	
    public static void sendFileToS3(String name, String path) throws IOException {
        AmazonS3 s3 = new AmazonS3Client(MainThread.credentials);
        Region reg;
        if(MainThread.Regs!=null)
        	reg = Region.getRegion(MainThread.Regs);
		else
			reg = Regions.getCurrentRegion();
        
        s3.setRegion(reg);

        String key = name;
        try {
            s3.putObject(new PutObjectRequest(BucketName, key, new File(path)));
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }
    
    public static void main(String[] args){
    	try {
			S3Manager.sendFileToS3("difficult.mp4", "/home/daniele/Scrivania/difficult.mp4");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
