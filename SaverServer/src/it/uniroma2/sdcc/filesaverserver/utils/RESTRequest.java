package it.uniroma2.sdcc.filesaverserver.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

public class RESTRequest {
	
	public static String RESTurl	= "http://rest.codep.quantumb.it/rest/recordings/";
	private static String token 	= "";
	
	public static void setToken(String t){
		token = t;
	}
	
	public static String sendInformations(String roomName, String url, Long size, String time) throws IOException, RESTException{
		HttpURLConnection connection;
		int responseCode;
		
		for (int i = 0; i < 20; i++) {
			try {
				connection = getConnection("PUT", RESTurl+roomName+"/");
				
				writeString(connection, "{\"available\":true, \"download_url\":\""+url+"\", \"length\":"+time+", \"dimension\":"+size+"}");
				
				responseCode = connection.getResponseCode();
				if (responseCode < 200 || responseCode > 299) {
					throw new RESTException("Invalid response code. " + responseCode + ": " + connection.getResponseMessage());
				}
				
				return readString(connection);
			} catch (SocketTimeoutException|ConnectException e) {
				System.out.println("Cannot connect to REST server.");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					throw new RESTException("Interrupted between two connection tries.");
				}
			}
		}
		
		throw new RESTException("Cannot connect to REST server.");
	}
	
	public static void deleteAfterError(String roomName) throws IOException, RESTException{
		HttpURLConnection hr;
		
		for (int i = 0; i < 5; i++) {
			try {
				hr = getConnection("DELETE", RESTurl+roomName+"/");
				int responseCode = hr.getResponseCode();
				if (responseCode < 200 || responseCode > 299) {
					throw new RESTException("Invalid response code. " + responseCode + ": " + hr.getResponseMessage());
				}
				
				return;
			} catch (SocketTimeoutException e) {
				System.out.println("Cannot connect to REST server.");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					throw new RESTException("Interrupted between two connection tries.");
				}
			}
		}
		
		throw new RESTException("Cannot connect to REST server.");
	}
	
	private static HttpURLConnection getConnection(String method, String url) throws IOException {
		URL urlObj;
		HttpURLConnection connection;
		
		urlObj = new URL(url);
		
		connection = (HttpURLConnection) urlObj.openConnection();
		connection.setRequestMethod(method);
		
		if (method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PUT")) {
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
		}

		connection.setDoInput(true);
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Authorization", "Token " + token);
		
		connection.setConnectTimeout(5000);
		
		return connection;
	}
	
	private static void writeString(HttpURLConnection connection, String message) throws IOException {
		OutputStream os = connection.getOutputStream();
		DataOutputStream dos = new DataOutputStream(os);
		dos.write(message.getBytes("UTF-8"));
		dos.write('\n');
		dos.flush();
		dos.close();
		os.close();
	}
	
	private static String readString(HttpURLConnection connection) throws IOException {
		InputStream content = connection.getInputStream();
		BufferedReader in = new BufferedReader (new InputStreamReader(content));
		String line;
		String string = "";
		while ((line = in.readLine()) != null) {
			string += line+"\n";
		}
		content.close();
		in.close();
		
		return string;
	}

}
