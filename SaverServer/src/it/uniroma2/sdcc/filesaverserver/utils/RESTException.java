package it.uniroma2.sdcc.filesaverserver.utils;

public class RESTException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7996085212484178961L;

	public RESTException(String detailMessage) {
		super(detailMessage);
	}

}
