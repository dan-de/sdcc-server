package it.uniroma2.sdcc.filesaverserver.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.FFMPEGLocator;
import it.sauronsoftware.jave.InputFormatException;
import it.sauronsoftware.jave.MultimediaInfo;

public class AudioConverter {
	
	public static final String SizeInfo 	= "Size";
	public static final String LengthInfo 	= "Length";
	public static final String FFMPEGpath 	= "/usr/bin/ffmpeg";
	
	public static void encoding(String inputFile, String outputFile) throws EncoderException, IOException{
		File source = new File(inputFile);
		File target = new File(outputFile);
		target.createNewFile();
		Encoder encoder = new Encoder(new CustomFFMPEGLocator());
		AudioAttributes aa = new AudioAttributes();
		aa.setCodec("libfaac");
		aa.setChannels(1);
		aa.setVolume(256);
		aa.setSamplingRate(44100);
		
		EncodingAttributes ea = new EncodingAttributes();
		ea.setAudioAttributes(aa);
		ea.setFormat("adts");
		
		encoder.encode(source, target, ea);
	}
	
	public static Map<String, Long> getFileInfo(String filePath) throws InputFormatException, EncoderException{
		File target = new File(filePath);
		Encoder e = new Encoder(new CustomFFMPEGLocator());
		MultimediaInfo mi = e.getInfo(target);
		long time = mi.getDuration();
		long size = target.length();
		
		HashMap<String, Long> infos = new HashMap<String, Long>();
		infos.put(LengthInfo, Long.valueOf(time));
		infos.put(SizeInfo, Long.valueOf(size));
		return infos;
	}
	
	public static class CustomFFMPEGLocator extends FFMPEGLocator{

		@Override
		protected String getFFMPEGExecutablePath() {
			return FFMPEGpath;
		}
		
	}

}
