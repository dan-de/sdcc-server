
#!/bin/bash

if [ "$(id -u)" != "0" ] ; then
    echo "You must be root to execute this script" 1>&2
    exit 2
fi

USER="ubuntu"
CREDFOLDER="/home/"$USER"/.aws"
CREDPATH=$CREDFOLDER/credentials

apt-get update
apt-get -f -y dist-upgrade

apt-get install -y git vim htop build-essential openjdk-7-jdk

sudo apt-add-repository ppa:jon-severinsson/ffmpeg 
sudo apt-get update
sudo apt-get install ffmpeg 

cd /opt

git clone https://dan-de@bitbucket.org/dan-de/sdcc-server.git

wget --user=dan-de --ask-password https://bitbucket.org/dan-de/sdcc-server/downloads/SaverServer.jar
touch token.txt

SERVER="sdcc-server/TCPServerLoadBalancing/saverServer.py"
EXECUT="saverServer.py"
SERVERTYPE="tcp-saver"

chmod +x /opt/$SERVER

FILE="/etc/init.d/$SERVERTYPE"
echo "#!/bin/bash" > $FILE
echo "### BEGIN INIT INFO" >> $FILE
echo "# Provides:				$SERVERTYPE" >> $FILE
echo "# Required-Start:		mountkernfs \$local_fs" >> $FILE
echo "# Required-Stop:		\$local_fs" >> $FILE
echo "# Default-Start:		2 3 4 5" >> $FILE
echo "# Default-Stop:			0 1 6" >> $FILE
echo "# Short-Description:	Start $SERVERTYPE service" >> $FILE
echo "### END INIT INFO" >> $FILE
echo "" >> $FILE
echo "USERNAME=\"ubuntu\"" >> $FILE
echo "SCRIPT=/opt/$SERVER" >> $FILE
echo "EXECUTABLE=\"$EXECUT\"" >> $FILE
echo "" >> $FILE
echo "sstart() {" >> $FILE
echo "	su - \$USERNAME -c \"screen -h 1024 -dmS $SERVERTYPE \$SCRIPT\"" >> $FILE
echo "	sleep 1" >> $FILE
echo "	echo \"Done.\"" >> $FILE
echo "}" >> $FILE
echo "" >> $FILE
echo "sstop() {" >> $FILE
echo "	kill -s SIGINT \$(ps ax | grep $EXECUTABLE | grep -v grep | awk '{print $1}')" >> $FILE
echo "	echo \"Done.\"" >> $FILE
echo "}" >> $FILE
echo "" >> $FILE
echo "case \"\$1\" in" >> $FILE
echo "	start)" >> $FILE
echo "		echo \"Starting...\"" >> $FILE
echo "		sstart" >> $FILE
echo "		;;" >> $FILE
echo "	stop)" >> $FILE
echo "		echo \"Stopping...\"" >> $FILE
echo "		sstop" >> $FILE
echo "		;;" >> $FILE
echo "	*)" >> $FILE
echo "		echo \"Usage: \$0 { start | stop }\"" >> $FILE
echo "		;;" >> $FILE
echo "esac" >> $FILE
echo "" >> $FILE
echo "exit 0" >> $FILE
echo "" >> $FILE

chmod +x $FILE

update-rc.d  $SERVERTYPE defaults

# Setting AWS Credentials
mkdir $CREDFOLDER

vim /opt/token.txt

cp /opt/sdcc-server/DecoderServer/scripts/credentials.skel $CREDPATH

vim $CREDPATH

chmod 600 $CREDPATH
chown -R $USER:$USER $CREDFOLDER

service $SERVERTYPE start

exit 0

