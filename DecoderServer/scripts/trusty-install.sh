
#!/bin/bash

if [ "$(id -u)" != "0" ] ; then
    echo "You must be root to execute this script" 1>&2
    exit 2
fi

USER="ubuntu"
CREDFOLDER="/home/"$USER"/.aws"
CREDPATH=$CREDFOLDER/credentials

apt-get update
apt-get -f -y dist-upgrade

apt-get install -y git vim htop build-essential openjdk-7-jdk \
maven libpoco-dev yasm pkg-config

cd /opt

wget https://libav.org/releases/libav-11.1.tar.gz

tar -xf libav-11.1.tar.gz

cd libav-11.1

./configure --enable-shared --disable-programs --disable-avdevice --disable-swscale --disable-avfilter --disable-avresample

make -j4

make install

cd ..

ln -s /usr/local/lib/libavcodec.so.56 /usr/lib/
ln -s /usr/local/lib/libavutil.so.54 /usr/lib/
ln -s /usr/local/lib/libavformat.so.56 /usr/lib/

git clone https://kudraw@bitbucket.org/dan-de/sdcc-server.git

vim sdcc-server/DecoderServer/Makefile

cd sdcc-server/DecoderServer/

make

FILE="/etc/init.d/sdcc-decoder"
echo "#!/bin/bash" > $FILE
echo "### BEGIN INIT INFO" >> $FILE
echo "# Provides:				sdcc-decoder" >> $FILE
echo "# Required-Start:		mountkernfs \$local_fs" >> $FILE
echo "# Required-Stop:		\$local_fs" >> $FILE
echo "# Default-Start:		2 3 4 5" >> $FILE
echo "# Default-Stop:			0 1 6" >> $FILE
echo "# Short-Description:	Start decoder service" >> $FILE
echo "### END INIT INFO" >> $FILE
echo "" >> $FILE
echo "USERNAME=\"ubuntu\"" >> $FILE
echo "SCRIPT=/opt/sdcc-server/DecoderServer/decoder" >> $FILE
echo "EXECUTABLE=\"decoder\"" >> $FILE
echo "" >> $FILE
echo "sstart() {" >> $FILE
echo "	su - \$USERNAME -c \"screen -h 1024 -dmS sdcc-decoder \$SCRIPT\"" >> $FILE
echo "	sleep 1" >> $FILE
echo "	echo \"Done.\"" >> $FILE
echo "}" >> $FILE
echo "" >> $FILE
echo "sstop() {" >> $FILE
echo "	kill -s SIGINT \$(pidof \$EXECUTABLE)" >> $FILE
echo "	echo \"Done.\"" >> $FILE
echo "}" >> $FILE
echo "" >> $FILE
echo "case \"\$1\" in" >> $FILE
echo "	start)" >> $FILE
echo "		echo \"Starting...\"" >> $FILE
echo "		sstart" >> $FILE
echo "		;;" >> $FILE
echo "	stop)" >> $FILE
echo "		echo \"Stopping...\"" >> $FILE
echo "		sstop" >> $FILE
echo "		;;" >> $FILE
echo "	*)" >> $FILE
echo "		echo \"Usage: \$0 { start | stop }\"" >> $FILE
echo "		;;" >> $FILE
echo "esac" >> $FILE
echo "" >> $FILE
echo "exit 0" >> $FILE
echo "" >> $FILE

chmod +x $FILE

update-rc.d sdcc-decoder defaults

# Setting AWS Credentials
mkdir $CREDFOLDER

cp scripts/credentials.skel $CREDPATH

vim $CREDPATH

chmod 600 $CREDPATH
chown -R $USER:$USER $CREDFOLDER

# Setting sdcc-decoder config
vim default.cfg

service sdcc-decoder start

# ln -s /opt/sdcc-server/DecoderServer/decoder /bin

exit 0

