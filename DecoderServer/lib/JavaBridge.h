/*
 * JavaBridge.h
 *
 *  Created on: Dec 24, 2014
 *      Author: giulio
 */

#ifndef LIB_JAVABRIDGE_H_
#define LIB_JAVABRIDGE_H_

#include <cassert>
#include <string>
#include <jni.h>
#include <JavaEnv.h>
#include <Poco/File.h>
#include <Poco/Path.h>
#include <Poco/Logger.h>
#include <Utils.h>
#include <Configuration.h>

namespace codep
{
	class JavaBridge final
	{
		static constexpr uint32_t MAX_CLASSPATH_STRLEN = 1024;

		JavaVM* jvm; /* denotes a Java VM */
		JNIEnv* mainEnv; /* pointer to native method interface */

		Poco::Logger& logger;

		std::string get_classpath_str(std::string folder)
		{
			std::vector<std::string> files;

			Poco::File(folder).list(files);

			std::vector<std::string> libs = {folder};

			for (auto& f : files) {
				if (Poco::Path(f).getExtension() == "jar")
					libs.push_back(f);
			}

			return join(libs, std::string(":").append(folder).append("/").c_str());
		}

		JavaBridge(std::string folder) : logger(Poco::Logger::get("JavaBridge"))
		{
			Configuration::getInstance().configureLogger(logger);

			char pathBuffer[MAX_CLASSPATH_STRLEN];

			std::string classPath = get_classpath_str(folder);

			assert( classPath.length() < MAX_CLASSPATH_STRLEN );

			poco_debug(logger, format("ClassPath: %s", classPath.c_str()));

			snprintf(pathBuffer, MAX_CLASSPATH_STRLEN, "-Djava.class.path=%s", classPath.c_str());

			JavaVMInitArgs vm_args; /* JDK/JRE 6 VM initialization arguments */
			JavaVMOption* options = new JavaVMOption[1];
			options[0].optionString = pathBuffer;
			vm_args.version = JNI_VERSION_1_6;
			vm_args.nOptions = 1;
			vm_args.options = options;
			vm_args.ignoreUnrecognized = false;

			/* load and initialize a Java VM, return a JNI interface pointer in env */
			JNI_CreateJavaVM(&jvm, (void**)&mainEnv, &vm_args);

			delete options;

			assert(jvm && mainEnv);
		}

		JavaBridge(const JavaBridge&) = delete; // disable copy
		JavaBridge(const JavaBridge&&) = delete; // disable move

	public:
		static JavaBridge& getInstance()
		{
			static JavaBridge instance(fullpath("classes"));

			return instance;
		}

		JavaEnv getEnv()
		{
			return mainEnv;
		}

		JavaEnv attachCurrentThread()
		{
			JNIEnv* threadEnv;

			jint res = jvm->AttachCurrentThread((void**) &threadEnv, NULL);

			if (res != JNI_OK)
				threadEnv = NULL;

			return threadEnv;
		}

		void detachCurrentThread()
		{
			jvm->DetachCurrentThread();
		}

		~JavaBridge()
		{
			jvm->DestroyJavaVM();
			poco_debug(logger, "JavaBridge destructed.");
		}
	};
}

#endif /* LIB_JAVABRIDGE_H_ */
