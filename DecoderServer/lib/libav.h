
#ifndef _LIBAV_H
#define _LIBAV_H

extern "C"
{
	#include <libavformat/avformat.h>
	#include <libavcodec/avcodec.h>
	#include <libavutil/common.h>
}

#endif
