
#ifndef LIB_JAVAENV_H_
#define LIB_JAVAENV_H_

#include <stdexcept>
#include <utility>
#include <jni.h>

namespace codep
{
	class JavaEnv
	{
		JNIEnv* env;

	public:
		JavaEnv(JNIEnv* _env)
		{
			if (_env == NULL)
				throw std::runtime_error("Trying to init JavaEnv with a NULL pointer");

			env = _env;
		}

		JNIEnv* operator->() {
			return env;
		}

		template<typename... Args>
		void call(const char* cls, const char* method, const char* signature, Args&&... args)
		{
			jclass _cls;
			jmethodID _method;

			_cls = env->FindClass(cls);

			if (cls == NULL) {
				throw std::runtime_error("JavaEnv::call => cannot find specified class");
			}

			_method = env->GetStaticMethodID(_cls, method, signature);

			if (_method == NULL) {
				throw std::runtime_error("JavaEnv::call => cannot find specified method");
			}

			env->CallStaticVoidMethod(_cls, _method, std::forward<Args>(args)...);

			env->DeleteLocalRef(_cls);
		}
	};
}

#endif /* LIB_JAVAENV_H_ */
