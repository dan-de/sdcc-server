/*
 * HttpClient.h
 *
 *  Created on: Dec 28, 2014
 *      Author: giulio
 */

#ifndef LIB_HTTPCLIENT_H_
#define LIB_HTTPCLIENT_H_

#include <sstream>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/URI.h>

namespace codep
{
	class HttpClient
	{
		static constexpr uint32_t RECEIVE_BUFFER_LEN = 1500;

		Poco::Net::HTTPClientSession session;
		std::string token;

		static std::string read_body(std::istream& streamBody);

	public:

		HttpClient(std::string hostname, std::string _token = "") : session(hostname),
																	token(_token)
		{}

		Poco::Net::HTTPResponse::HTTPStatus get(std::string url, std::string& responseBody);
		Poco::Net::HTTPResponse::HTTPStatus post(std::string url, std::string body, std::string& responseBody);
		Poco::Net::HTTPResponse::HTTPStatus del(std::string url, std::string& responseBody);
		Poco::Net::HTTPResponse::HTTPStatus head(std::string url, std::string token);
	};
}



#endif /* LIB_HTTPCLIENT_H_ */
