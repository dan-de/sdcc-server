/*
 * DecoderRunnable.h
 *
 *  Created on: Dec 21, 2014
 *      Author: giulio
 */

#ifndef LIB_DECODERRUNNABLE_H_
#define LIB_DECODERRUNNABLE_H_

#include <libav.h>
#include <CodecContext.h>
#include <FormatContext.h>
#include <Poco/Runnable.h>
#include <Poco/Logger.h>
#include <Configuration.h>
#include <Utils.h>

namespace codep
{
	class DecoderRunnable : public Poco::Runnable
	{
		static constexpr int32_t BYTES_PER_CHANNEL = 2; // Mono PCM
		static constexpr int32_t HZ = 44100; // Sample Rate
		static constexpr int32_t BYTES_PER_SECOND = HZ*BYTES_PER_CHANNEL;
		static constexpr int32_t MAX_CHUNK_BYTES = 261072; // 255*1024; // bytes (for SQS message)
		static constexpr int32_t MAX_SAMPLES = MAX_CHUNK_BYTES/BYTES_PER_CHANNEL;
		static constexpr int32_t TICK_PROGRESS_MS = MAX_CHUNK_BYTES/(BYTES_PER_SECOND/1000.0);

		AVCodec* codec;
		CodecContext codec_ctx;
		std::string udpUrl;
		std::string queueName;
		volatile bool request_termination;
		Poco::Logger& logger;
		bool dirty;
		int64_t ticks;

		static int interrupt_callback(void* args)
		{
			DecoderRunnable* instance = static_cast<DecoderRunnable*>(args);
			if (instance->request_termination) {
				if (!instance->dirty) {
					poco_debug(instance->logger, "DecoderRunnable has no more data, terminate.");

					return 1;
				}

				instance->dirty = false;
			}

			return 0;
		}

	public:

		DecoderRunnable(std::string _url, std::string _queueName, int64_t _ticks) :
			codec(CodecContext::getCodec(AV_CODEC_ID_AAC)),
			codec_ctx(codec),
			udpUrl(_url),
			queueName(_queueName),
			request_termination(false),
			logger(Poco::Logger::get("DecoderRunnable")),
			dirty(false),
			ticks(_ticks)
		{
			Configuration::getInstance().configureLogger(logger);

			poco_debug(logger, format("Socket Url: %s", udpUrl.c_str()));
			poco_debug(logger, format("SQS Queue Name: %s", queueName.c_str()));
			poco_debug(logger, format("Tick progress rate (ms): %d", TICK_PROGRESS_MS));
		}

		DecoderRunnable(const DecoderRunnable&) = delete; // disable copy
		DecoderRunnable(const DecoderRunnable&&) = delete; // disable move

		virtual void run();

		void stop() {
			request_termination = true;
		}
	};
}


#endif /* LIB_DECODERRUNNABLE_H_ */
