/*
 * Configuration.h
 *
 *  Created on: Jan 7, 2015
 *      Author: giulio
 */

#ifndef LIB_CONFIGURATION_H_
#define LIB_CONFIGURATION_H_

#include <string>
#include <Poco/Util/PropertyFileConfiguration.h>
#include <Poco/Channel.h>
#include <Poco/ConsoleChannel.h>
#include <Poco/SyslogChannel.h>
#include <Poco/AutoPtr.h>
#include <Utils.h>


namespace codep
{
	class Configuration final
	{
		Poco::AutoPtr<Poco::Util::PropertyFileConfiguration> config;
		Poco::Channel* logChannel;
		std::string logLevel;

		Configuration(std::string configFile) : config(new Poco::Util::PropertyFileConfiguration(fullpath(configFile))),
												LISTEN_PORT(config->getInt("tcp.listen_port", 9999)),
												READ_TIMEOUT(config->getInt("tcp.read_timeout", 60)),
												GOD_TOKEN(config->getString("rest.admin_token", ""))
		{
			if (config->getString("log.target", "") == "syslog") {
				logChannel = new Poco::SyslogChannel("sdcc-decoder");
			}
			else {
				logChannel = new Poco::ConsoleChannel;
			}

			logLevel = config->getString("log.level", "informational");
		}

	public:
		const int LISTEN_PORT;
		const int READ_TIMEOUT;
		const std::string GOD_TOKEN;

		static Configuration& getInstance(std::string configFile = "")
		{
			static Configuration instance(configFile);

			return instance;
		}

		Configuration(const Configuration&) = delete; // disable copy
		Configuration(const Configuration&&) = delete; // disable move

		void configureLogger(Poco::Logger& logger) const
		{
			logger.setChannel(logChannel);
			logger.setLevel(logLevel);
		}
	};
}


#endif /* LIB_CONFIGURATION_H_ */
