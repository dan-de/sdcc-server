/*
 * FormatContext.h
 *
 *  Created on: Dec 21, 2014
 *      Author: giulio
 */

#ifndef LIB_FORMATCONTEXT_H_
#define LIB_FORMATCONTEXT_H_

#include <libav.h>

namespace codep
{
	class FormatContext final
	{
		AVFormatContext* ctx;

	public:

		FormatContext(std::string url, int(*int_callback)(void*) = NULL, void* opaque = NULL) {
			ctx = avformat_alloc_context();

			if (int_callback) {
				ctx->interrupt_callback.callback = int_callback;
				ctx->interrupt_callback.opaque = opaque;
			}

			int err = avformat_open_input(&ctx, url.c_str(), NULL, NULL);

			if (err < 0)
			{
				char errbuff[512] = {0};

				av_strerror(err, errbuff, 512);

				std::string error(errbuff);

				std::cerr << error << "\n";

				throw std::runtime_error(std::string("FormatContext: unable to open url => ").append(error));
			}
		}

		FormatContext(const FormatContext&) = delete; // disable copy
		FormatContext(const FormatContext&&) = delete; // disable move

		bool readPacket(AVPacket* pkt) {
			return av_read_frame(ctx, pkt) == 0;
		}

		~FormatContext()
		{
			avformat_close_input(&ctx);
		}
	};
}



#endif /* LIB_FORMATCONTEXT_H_ */
