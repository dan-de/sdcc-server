
#ifndef LIB_BASE64_H_
#define LIB_BASE64_H_

#include <cstddef>
#include <cstdint>
#include <cmath>

size_t base64_encode_ushort(const char* __restrict in, size_t inlen, uint16_t* __restrict out, size_t outlen);

static constexpr int BIN_TO_B64_SIZE(double bytes)
{
	return 4*std::ceil(bytes/3);
}

static constexpr int B64_TO_BIN_SIZE(int bytes)
{
	return 3*(bytes/4);
}


#endif /* LIB_BASE64_H_ */
