/*
 * KeepAliveThread.h
 *
 *  Created on: Jan 27, 2015
 *      Author: giulio
 */

#ifndef LIB_KEEPALIVERUNNABLE_H_
#define LIB_KEEPALIVERUNNABLE_H_

#include <Poco/Runnable.h>
#include <Poco/Net/StreamSocket.h>
#include <Poco/Thread.h>

namespace codep
{
	class KeepAliveRunnable : public Poco::Runnable
	{
		Poco::Net::StreamSocket& socket;
		int interval;
		volatile bool running;
	public:
		KeepAliveRunnable(Poco::Net::StreamSocket& _socket, int _interval) : socket(_socket),
		                                                                     interval(_interval),
		                                                                     running(false)
		{
		}

		virtual void run()
		{
			running = true;

			uint8_t buffer[] = { 42 };

			while (running && socket.sendBytes(buffer, 1) > 0) {
				Poco::Thread::sleep(interval);
			}
		}

		void stop()
		{
			running = false;
		}
	};
}


#endif /* LIB_KEEPALIVERUNNABLE_H_ */
