/*
 * ConnectionHandler.h
 *
 *  Created on: Dec 21, 2014
 *      Author: giulio
 */

#ifndef SRC_CONNECTIONHANDLER_H_
#define SRC_CONNECTIONHANDLER_H_

#include <iostream>
#include <string>

#include <Poco/Net/TCPServer.h>
#include <Poco/Net/DatagramSocket.h>
#include <Poco/Thread.h>
#include <Poco/ByteOrder.h>
#include <Poco/Logger.h>
#include <rapidjson/document.h>

#include <DecoderRunnable.h>
#include <HttpClient.h>
#include <Utils.h>
#include <Configuration.h>
#include <KeepAliveRunnable.h>

#include <atomic>

#include <sys/socket.h>
#include <sys/un.h>

#include <chrono>

class ConnectionHandler : public Poco::Net::TCPServerConnection {

	static constexpr int BUFFER_SIZE = 8192;
	static constexpr int KEEP_ALIVE_INTERVAL = 4000;
	static constexpr char* COORDINATOR_URL = "rest.codep.quantumb.it";
	static constexpr char* ROOMS_URL = "/rest/rooms/";
	static constexpr char* JOIN_URL_TEMPLATE = "/rest/rooms/%s/users/";
	static constexpr char* LEAVE_URL_TEMPLATE = "/rest/rooms/%s/users/%s/?url=%s";
	static constexpr char* JOIN_CRED_TEMPLATE = "{\"username\":\"%s\"}";
	static constexpr char* UNIX_PATH_TEMPLATE = "/tmp/decoder-%u";

	static std::atomic<uint32_t> global_counter;

	Poco::Thread decoder_thread;
	std::string originalQueueUrl;
	const std::string CLIENT;
	Poco::Logger& logger;
	int64_t ticks;

	/**
	 * Generate next port in unix ephemeral port range (32000 - 65535)
	 *
	 * To generate next port across various threads, a global counter is
	 * increased atomically each time, and the old value will be taken.
	 * Then this value is used to compute next port.
	 *
	 * To make modulo operation faster, we use a power of two: 32768.
	 *
	 * This will sightly reduce ephemeral port range to (32000 - 64767)
	 *
	 * @return Next port in range (32000 - 64767)
	 */
	uint16_t get_next_port()
	{
		return 32000 + (global_counter++ % 32768);
	}

	/**
	 * Read credentials data from connection socket into a buffer
	 *
	 * The buffer will contain the JSON with credentials username, token and roomId
	 *
	 * @return -1 on error, 0 success
	 */
	int read_credentials(Poco::Net::StreamSocket& socket, uint8_t* buffer)
	{
		int rd;
		int32_t* credLenPtr;
		int32_t credLen;
		Poco::Timespan timeout(10, 0);
		socket.setReceiveTimeout(timeout);

		try {
			rd = socket.receiveBytes(buffer, sizeof(int32_t));

			if (rd != sizeof(int32_t)) {
				poco_debug(logger, CLIENT+codep::format("Credential Length Size: %d", rd));
				goto cleanup;
			}

			credLenPtr = reinterpret_cast<int32_t*>(buffer);

			credLen = Poco::ByteOrder::fromBigEndian(*credLenPtr);

			rd = socket.receiveBytes(buffer, credLen);

			if (rd != credLen) {
				poco_debug(logger, CLIENT+codep::format("Credential Length Read: %d", rd));
				goto cleanup;
			}
		}
		catch (const Poco::TimeoutException& tex) {
			poco_notice(logger, CLIENT+"Timeout while reading credentials.");
			goto cleanup;
		}

		buffer[rd] = '\0';

		return 0;

	cleanup:
		poco_error(logger, CLIENT+"Connection closed, reason: protocol error while reading credentials");
		socket.close();
		return -1;
	}


	int create_unix_socket(std::string path) {
		struct sockaddr_un decoder_addr;

		// Prepare Socket Address
		memset(&decoder_addr, 0, sizeof(decoder_addr));
		decoder_addr.sun_family = AF_UNIX;
		strncpy(decoder_addr.sun_path, path.c_str(), 108);

		int sock = ::socket(AF_UNIX, SOCK_STREAM, 0);

		unlink(decoder_addr.sun_path);

		if (bind(sock, (const sockaddr*) &decoder_addr, sizeof(decoder_addr)) < 0) {
			perror("create_unix_socket::bind => ");
		}

		if (listen(sock, 3) < 0) {
			perror("listen:");
		}

		return sock;
	}

public:
	ConnectionHandler(const Poco::Net::StreamSocket& _socket) :
		Poco::Net::TCPServerConnection(_socket),
		CLIENT("["+_socket.peerAddress().toString()+"] "),
		logger(Poco::Logger::get("ConnectionHandler"))
	{
		using namespace std::chrono;
		codep::Configuration::getInstance().configureLogger(logger);

		auto current_time = system_clock::now().time_since_epoch();

		ticks = duration_cast<milliseconds>(current_time).count();

		poco_information(logger, CLIENT+"Connected.");
	}


	virtual void run()
	{
		using namespace codep;

		int rd = -1;
		std::string user_token;
		std::string room;
		std::string user;
		std::string unix_sock_path;
		std::string USER = CLIENT;
		rapidjson::Document jsonParser;
		std::string response;
		uint32_t responseCode;
		int listen_sock;
		int decoder_sock;
		uint8_t buffer[BUFFER_SIZE];

		Poco::Timespan timeout(Configuration::getInstance().READ_TIMEOUT, 0);

#ifndef _NET_OUTPUT_OFF
		// 1) Read token, username and room from client
		if (read_credentials(socket(), buffer) < 0)
			return;

		jsonParser.Parse(reinterpret_cast<const char*>(buffer));

		user_token = jsonParser["token"].GetString();
		room = jsonParser["roomId"].GetString();
		user = jsonParser["username"].GetString();

		poco_debug(logger, CLIENT+"Creds: " + user_token + ", " + room + ", " + user);

		USER = format("[%s]", user.c_str());

		// 2) Send REST query to Coordinator
		HttpClient client(COORDINATOR_URL, Configuration::getInstance().GOD_TOKEN);

		responseCode = client.head(ROOMS_URL, user_token);

		// 3) Wait for response, if token is valid a 200 OK response will be received,
		//    otherwise a 403 Forbidden will be triggered
		if (responseCode != 200) {
			poco_error(logger, USER+format("Authentication failed, coordinator response code: %u", responseCode));
			socket().close();
			return;
		}

		responseCode = client.post(format(JOIN_URL_TEMPLATE, room.c_str()),
		                           format(JOIN_CRED_TEMPLATE, user.c_str()),
		                           response);

		if (responseCode != 200) {
			poco_error(logger, USER+format("Join failed, coordinator response code: %u", responseCode));
			socket().close();
			return;
		}

		// 4) Parse JSON response
		jsonParser.Parse(response.c_str());

		originalQueueUrl = jsonParser["user_queue_url"].GetString();

		size_t index = originalQueueUrl.rfind('/');

		if (index == originalQueueUrl.npos) {
			poco_error(logger, USER+"Connection closed, reason: invalid user_queue_url");
			socket().close();
			return;
		}

#else
		size_t index = -1;
		originalQueueUrl = "network_off";
#endif
		// 5) Response is ok, then allocate an UNIQUE_ID and create unix socket

		unix_sock_path = format(UNIX_PATH_TEMPLATE, global_counter++);

		listen_sock = create_unix_socket(unix_sock_path);

		// 6) Init decoder runnable
		DecoderRunnable decoder(format("unix://%s", unix_sock_path.c_str()),
		                        originalQueueUrl.substr(index+1),
		                        ticks);

		decoder_thread.start(decoder);

		// 6b) Init keep alive runnable
		codep::KeepAliveRunnable keep_alive(socket(), KEEP_ALIVE_INTERVAL);
		Poco::Thread keeper;
		keeper.start(keep_alive);

		// 7) forward data from tcp to udp decode chain
		socket().setReceiveTimeout(timeout);

		decoder_sock = accept(listen_sock, NULL, NULL);

		if (decoder_sock < 0)
			perror("accept:");

		try {
			while((rd = socket().receiveBytes(buffer, BUFFER_SIZE)) > 0) {
				if (write(decoder_sock, buffer, rd) < 0) {
					poco_critical(logger, USER+"Error writing to local socket");
					break;
				}
			}
		}
		catch (const Poco::TimeoutException& tex) {
			poco_notice(logger, USER+"Timeout.");
			socket().close();
		}

		poco_debug(logger, USER+format("Exit from receive loop, rd was: %d",rd));

		close(decoder_sock);
		decoder_thread.join();
		keep_alive.stop();
		keeper.join();

		poco_information(logger, USER+"Threads stopped.");

		unlink(unix_sock_path.c_str());

#ifndef _NET_OUTPUT_OFF
		std::string deleteUrl = format(LEAVE_URL_TEMPLATE,
									   room.c_str(),
									   user.c_str(),
									   originalQueueUrl.c_str());

		responseCode = client.del(deleteUrl, response);

		if (responseCode != 204) {
			poco_error(logger, USER+"Error while deleting room: "+response);
		}
#endif

		poco_information(logger, USER+"Connection closed.");
	}
};

#endif /* SRC_CONNECTIONHANDLER_H_ */
