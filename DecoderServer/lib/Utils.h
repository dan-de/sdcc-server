
#ifndef LIB_UTILS_H_
#define LIB_UTILS_H_

#include <string>
#include <sstream>
#include <vector>
#include <unistd.h>

namespace codep
{
	template<typename T>
	static std::string join(const std::vector<T>& l, const char* delim = " ")
	{
		std::stringstream ss;

		const char* separator = "";

		for (auto& e : l)
		{
			ss << separator << e;
			separator = delim;
		}

		return ss.str();
	}

	template<size_t MaxSize = 1024, typename... Args>
	static std::string format(const char* format, Args&&... args)
	{
		char buffer[MaxSize];

		snprintf(buffer, MaxSize, format, std::forward<Args>(args)...);

		return buffer;
	}

	static std::string fullpath(std::string file="")
	{
		static constexpr size_t MAX_PATH_LEN = 512;

		char buffer[MAX_PATH_LEN];

		ssize_t len = readlink("/proc/self/exe", buffer, MAX_PATH_LEN);

		if (len != -1) {
			char* ptr = &buffer[len-1];

			// strip away executable name from path
			while (*ptr != '/') {
				ptr--;
				len--;
			}

			if (file.length() > 0) {
				strncpy(ptr+1, file.c_str(), file.length());
				len += file.length();
			}
			else {
				len--; // no string to copy: removing trailing '/'
			}
		}
		else {
			len = 0; // empty string
		}

		buffer[len] = '\0';

		return buffer;
	}
}


#endif /* LIB_UTILS_H_ */
