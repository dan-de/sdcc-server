/*
 * CodecContext.h
 *
 *  Created on: Dec 21, 2014
 *      Author: giulio
 */

#ifndef LIB_CODECCONTEXT_H_
#define LIB_CODECCONTEXT_H_

#include <stdexcept>
#include <libav.h>
#include <Frame.h>
#include <iostream>
#include <mutex>

namespace codep
{
	class CodecContext final
	{
		AVCodecContext* ctx;

		Frame outFrame;

		static std::mutex avcodec_mutex;

	public:

		static AVCodec* getCodec(AVCodecID codecId) {
			return avcodec_find_decoder(codecId);
		}

		CodecContext(const AVCodec* codec) {
			ctx = avcodec_alloc_context3(codec);

			if (ctx == NULL)
				throw std::runtime_error("CodecContex: unable to allocate context.");

			std::lock_guard<std::mutex> lock(avcodec_mutex);

			if (avcodec_open2(ctx, codec, NULL) < 0) {
				throw std::runtime_error("CodecContext: unable to open codec.");
			}
		}

		CodecContext(const CodecContext&) = delete; // disable copy
		CodecContext(const CodecContext&&) = delete; // disable move

		Frame& decode(AVPacket* source);

		~CodecContext() {
			std::lock_guard<std::mutex> lock(avcodec_mutex);
			avcodec_close(ctx);
			avcodec_free_context(&ctx);
		}
	};
}

#endif /* LIB_CODECCONTEXT_H_ */
