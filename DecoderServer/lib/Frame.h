/*
 * Frame.h
 *
 *  Created on: Dec 21, 2014
 *      Author: giulio
 */

#ifndef LIB_FRAME_H_
#define LIB_FRAME_H_

#include <stdexcept>
#include <libav.h>

namespace codep
{
	class Frame final
	{
		AVFrame* avframe;
		bool valid;
		mutable int index;

		friend class CodecContext;

		Frame() : valid(false), index(0)
		{
			avframe = av_frame_alloc();

			if (avframe == NULL)
				throw std::runtime_error("Frame: unable to allocate.");
		}

public:
		Frame(const Frame&) = delete; // disable copy
		Frame(const Frame&&) = delete; // disable move

		int read(int16_t* samples, size_t len) const {
			float* channel = (float*) avframe->extended_data[0];

			size_t i;

			for (i=0; i < len && index < avframe->nb_samples; i++, index++)
				samples[i] = (int16_t) (channel[index] * 32767.0f);

			return i;
		}

		void getPCM(int16_t* samples) const {
			float* inputChannel0 = (float*) avframe->extended_data[0];

			for (int i=0; i < avframe->nb_samples; i++)
			{
				samples[i] = (int16_t) (inputChannel0[i] * 32767.0f);
			}
		}

		int getSamplesNo() const {
			return valid ? avframe->nb_samples : 0;
		}

		void release() {
			av_frame_unref(avframe);
			valid = false;
			index = 0;
		}

		~Frame() {
			av_frame_free(&avframe);
		}
	};
}


#endif /* LIB_FRAME_H_ */
