
#include <DecoderRunnable.h>
#include <wavfile.h>
#include <iostream>
#include <cassert>
#include <JavaBridge.h>
#include <JavaEnv.h>

using namespace codep;

static inline void sqs_send(JavaEnv& env,
                            std::string queueUrl,
                            uint8_t* samples,
                            size_t len,
                            int chunkId,
                            int64_t ticks,
                            int flags = 0)
{
#ifndef _NET_OUTPUT_OFF
	jstring url = env->NewStringUTF(queueUrl.c_str());
	jobject buff = env->NewDirectByteBuffer(samples, len);

	env.call("SendMessage",
	         "sendMessage",
	         "(Ljava/lang/String;ILjava/nio/ByteBuffer;IJ)V",
	         url,
	         chunkId,
	         buff,
	         flags,
	         ticks);

	jthrowable ex = env->ExceptionOccurred();

	if (ex != NULL) {
		env->ExceptionDescribe();
		env->ExceptionClear();
	}

	env->DeleteLocalRef(buff);
	env->DeleteLocalRef(url);
#endif
}

void DecoderRunnable::run()
{
	// FILE* output = NULL;
	AVPacket pkt;

	JavaEnv env = JavaBridge::getInstance().attachCurrentThread();

	FormatContext fm_ctx(udpUrl);

	av_init_packet(&pkt);

	// output = wavfile_open(udpUrl.substr(7).append(".wav").c_str());

	int chunks = 0;

	int samples_no = 0;

	int16_t* samples = new int16_t[MAX_SAMPLES];

	while (fm_ctx.readPacket(&pkt))
	{
		Frame& frame = codec_ctx.decode(&pkt);

		if (frame.getSamplesNo() == 0) continue;

		// got valid data
		dirty = true;

		int rd;

		while ( (rd=frame.read(samples+samples_no, MAX_SAMPLES-samples_no)) > 0)
		{
			samples_no += rd;

			if (samples_no >= MAX_SAMPLES)
			{
				assert(samples_no <= MAX_SAMPLES);

				// wavfile_write(output, samples, MAX_SAMPLES);

				// 1) Send to SQS
				sqs_send(env, queueName, reinterpret_cast<uint8_t*>(samples), MAX_SAMPLES*2, chunks, ticks);

				// 2) Increase chunks number
				++chunks;

				poco_debug(logger, format("Frames committed at %ld for chunk %d: %d", ticks, chunks-1, samples_no));

				// 2b) Increase ticks
				ticks += TICK_PROGRESS_MS;

				// 3) Reset samples counter
				samples_no = 0;
			}
		}

		frame.release();

		av_free_packet(&pkt);
	}

	// flush the remaining buffer
	if (samples_no > 0)
	{
		sqs_send(env, queueName, reinterpret_cast<uint8_t*>(samples), samples_no*2, chunks, ticks, 1);
		// wavfile_write(output, samples, samples_no);
		++chunks;

		poco_debug(logger, format("Frames flushed at end: %d", samples_no));
	}

	// wavfile_close(output);

	delete[] samples;

	poco_information(logger, format("Total chunks processed: %d", chunks));
	poco_debug(logger, "DecoderThread quit.");

	JavaBridge::getInstance().detachCurrentThread();
}
