

#include <CodecContext.h>

using namespace codep;

std::mutex CodecContext::avcodec_mutex;

Frame& CodecContext::decode(AVPacket* source)
{
	int gotframe = 0;

	if (avcodec_decode_audio4(ctx, outFrame.avframe, &gotframe, source) < 0)
	{
		outFrame.valid = false;
	}

	if (gotframe)
		outFrame.valid = true;

	return outFrame;
}
