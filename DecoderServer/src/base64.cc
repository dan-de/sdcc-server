
#include <base64.h>

/* C89 compliant way to cast 'char' to 'unsigned char'. */
static inline unsigned char to_uchar (char ch)
{
	return ch;
}

size_t
base64_encode_ushort(const char* __restrict in, size_t inlen, uint16_t* __restrict out, size_t outlen)
{
	static const char b64str[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	size_t maxlen = outlen;

	while (inlen && outlen)
	{
		*out++ = b64str[(to_uchar (in[0]) >> 2) & 0x3f];
		if (!--outlen)
			break;
		*out++ = b64str[((to_uchar (in[0]) << 4)
				+ (--inlen ? to_uchar (in[1]) >> 4 : 0))
						& 0x3f];
		if (!--outlen)
			break;
		*out++ =
				(inlen
						? b64str[((to_uchar (in[1]) << 2)
								+ (--inlen ? to_uchar (in[2]) >> 6 : 0))
								 & 0x3f]
								 : '=');
		if (!--outlen)
			break;
		*out++ = inlen ? b64str[to_uchar (in[2]) & 0x3f] : '=';
		if (!--outlen)
			break;
		if (inlen)
			inlen--;
		if (inlen)
			in += 3;
	}

	if (outlen)
		*out = '\0';

	return maxlen - outlen;
}
