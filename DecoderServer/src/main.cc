
#include <stdio.h>
#include <stdint.h>


#include <libav.h>
#include <wavfile.h>
#include <signal.h>

#include <Poco/ThreadPool.h>
#include <Poco/Net/TCPServer.h>

#include <ConnectionHandler.h>
#include <Configuration.h>

using namespace Poco;
using namespace Poco::Net;
using namespace codep;

static constexpr int POOL_THREADS_INC = 1024;

static sigset_t accepted_signals;

static void av_init()
{
	av_register_all();
	avcodec_register_all();
	avformat_network_init();
}

static void av_destroy()
{
	avformat_network_deinit(); // leaks 40 bytes :(
}

static void install_sighandler()
{
	struct sigaction sa;

	memset(&sa, 0, sizeof(sa));

	sigemptyset(&accepted_signals);

	sigaddset(&accepted_signals, SIGINT);

	sa.sa_handler = SIG_HOLD;
	sa.sa_mask = accepted_signals;

	sigaction(SIGINT, &sa, NULL);
}


int main(void)
{
	Configuration& config = Configuration::getInstance("default.cfg");

	Logger& logger = Logger::get("Main");

	config.configureLogger(logger);

	int sig_recv;
	install_sighandler();
	av_init();

	ThreadPool::defaultPool().addCapacity(POOL_THREADS_INC);

	ServerSocket server_sock(config.LISTEN_PORT);

	auto factory = new TCPServerConnectionFactoryImpl<ConnectionHandler>();

	TCPServer server(factory, server_sock);
	// NOTE: server destruction cause factory destruction,
	//       no delete factory required (or double free happens) ;).

	server.start();

#ifdef _NET_OUTPUT_OFF
	poco_information(logger, "Network output switched off.");
#endif

	poco_information(logger, format("Start listening on port %hu", server.port()));

	sigwait(&accepted_signals, &sig_recv);

	server.stop();

	while (server.currentConnections() > 0)
		;

	poco_information(logger, "Stopped");

	av_destroy(); // leaks some memory, see body to details

    return 0;
}
