
#include <HttpClient.h>

using namespace codep;
using namespace Poco;
using namespace Poco::Net;

std::string HttpClient::read_body(std::istream& streamBody)
{
	std::stringstream ss;
	char responseBuffer[RECEIVE_BUFFER_LEN];

	while (!streamBody.eof())
	{
		streamBody.read(responseBuffer, RECEIVE_BUFFER_LEN);

		responseBuffer[streamBody.gcount()] = '\0';

		ss << responseBuffer;
	}

	return ss.str();
}

HTTPResponse::HTTPStatus HttpClient::get(std::string url, std::string& responseBody)
{
	HTTPResponse response;
	HTTPRequest request(HTTPRequest::HTTP_GET, url, HTTPRequest::HTTP_1_1);

	if (!token.empty())
		request.setCredentials("Token", token);

	request.set("Accept", "application/json");
	request.set("Accept-Encoding", "gzip, deflate");

	session.sendRequest(request);

	std::istream& streamBody = session.receiveResponse(response);

	responseBody = read_body(streamBody);

	return response.getStatus();
}

HTTPResponse::HTTPStatus HttpClient::post(std::string url, std::string body, std::string& responseBody)
{
	HTTPResponse response;
	HTTPRequest request(HTTPRequest::HTTP_POST, url, HTTPRequest::HTTP_1_1);

	if (!token.empty())
		request.setCredentials("Token", token);

	request.setContentType("application/json");
	request.setContentLength(body.length());

	std::ostream& requestBody = session.sendRequest(request);

	requestBody << body;

	std::istream& streamBody = session.receiveResponse(response);

	responseBody = read_body(streamBody);

	return response.getStatus();
}

HTTPResponse::HTTPStatus HttpClient::del(std::string url, std::string& responseBody)
{
	HTTPResponse response;
	HTTPRequest request(HTTPRequest::HTTP_DELETE, url, HTTPRequest::HTTP_1_1);

	if (!token.empty())
			request.setCredentials("Token", token);

	request.setContentType("application/json");

	session.sendRequest(request);

	std::istream& streamBody = session.receiveResponse(response);

	responseBody = read_body(streamBody);

	return response.getStatus();
}

HTTPResponse::HTTPStatus HttpClient::head(std::string url, std::string token)
{
	HTTPResponse response;
	HTTPRequest request(HTTPRequest::HTTP_HEAD, url, HTTPRequest::HTTP_1_1);

	if (!token.empty())
		request.setCredentials("Token", token);

	request.set("Accept", "application/json");
	request.set("Accept-Encoding", "gzip, deflate");

	session.sendRequest(request);

	session.receiveResponse(response);

	return response.getStatus();
}
