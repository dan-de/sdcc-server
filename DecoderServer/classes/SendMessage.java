import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import java.nio.ByteBuffer;

public class SendMessage {
	
	public static final String TokenTag = "Token";
	public static final String ChunkTag = "ChunkId";
    public static final String DataTag = "Data";
    public static final String FlagsTag = "Flags";
    public static final String TimestampTag = "Timestamp";

    public static AmazonSQS sqs;
    public static final Regions actual = Regions.EU_CENTRAL_1;

    static {
    	AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            System.out.println("Exception catched while acquiring credentials.");
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format.",
                    e);
        }

        System.err.println("Amazon Credentials Acquired.");

        sqs = new AmazonSQSClient(credentials);
        
        sqs.setRegion(Region.getRegion(actual));
    }


	public static void sendMessage(String queueName, int chunkId, ByteBuffer data, int flags, long ticks) {
		String url;

        // System.err.println("Received chunk " + chunkId + ", len: " + data.capacity());

        url = sqs.getQueueUrl(queueName).getQueueUrl();

        System.out.println("Retreived Queue URL: " + url);
        
        // Send a message
        // System.out.println("Sending a message to the queue.\n");
        SendMessageRequest smr = new SendMessageRequest();
        MessageAttributeValue mav = new MessageAttributeValue();
        
        smr.setQueueUrl(url);
        smr.setMessageBody("data");

        mav.setBinaryValue(data);
        mav.setDataType("Binary");
        smr.addMessageAttributesEntry(DataTag, mav);
        
        // mav.setStringValue(userId);
        // mav.setDataType("String");
        // smr.addMessageAttributesEntry(TokenTag, mav);
        mav = new MessageAttributeValue();
        mav.setStringValue(Integer.toString(chunkId));
        mav.setDataType("Number");
        smr.addMessageAttributesEntry(ChunkTag, mav);
        
        mav = new MessageAttributeValue();
        mav.setStringValue(Integer.toString(flags));
        mav.setDataType("Number");
        smr.addMessageAttributesEntry(FlagsTag, mav);

        mav = new MessageAttributeValue();
        mav.setStringValue(Long.toString(ticks));
        mav.setDataType("Number");
        smr.addMessageAttributesEntry(TimestampTag, mav);

        sqs.sendMessage(smr);
	}
}
