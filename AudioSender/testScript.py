#!/usr/bin/python
import os
import sys
import csv

if len(sys.argv)!=6:
	print "Usage: python scriptTest.py <SDCC_TCP_SERVER> <Files directory> <Executable> <RoomID> <CSV path>;"
	exit(1)
	
#os.system("export SDCC_TCP_SERVER="+sys.argv[1])

filedir=sys.argv[2]
executable=sys.argv[3]
roomid=sys.argv[4]
csvfile=sys.argv[5]

if not filedir.endswith('/'):
	filedir=filedir+"/"
	
csvopen = open(csvfile, 'rb')

files = os.listdir(filedir)

try:
	reader = csv.reader(csvopen)
	for i in files:
		j = reader.next()
		if j==None:
			break
		os.system("export SDCC_TCP_SERVER="+sys.argv[1]+" && java -jar "+executable+" -i "+filedir+i+" -r "+roomid+" -u "+str(j[0])+" -t "+str(j[1])+" &")
finally:
	csvopen.close()
