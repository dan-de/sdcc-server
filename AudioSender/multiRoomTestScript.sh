#!/bin/bash

REST="http://rest.codep.quantumb.it"
ROOMS="/rest/rooms/"

TESTSCRIPT="./testScript.py"

if [[ $(echo "($# < 4)" | bc) == 1 || $(echo "($# - 2) % 2" | bc) != 0 ]] ; then
	echo "Usage: $0 SDCC_TCP_SERVER Executable Files_Directory CSV_Path [Files_Directory CSV_Path] [Files_Directory CSV_Path] ..."
	exit 1
fi

if [ ! $TOKEN ] ; then
	echo "You must export TOKEN variable first!"
	echo "Use: export TOKEN=\"1234567890abcdef\""
	echo "This token will be used for room creation"
	exit 1
fi

get_new_room() {
	JSON=$(curl -s -X POST $REST$ROOMS -H "Content-Type: application/json" -H "Authorization: Token $TOKEN" -d "{\"latitude\":41.854544, \"longitude\":12.623972, \"roomName\":\"$1\"}")
	echo $JSON | python -c "import json,sys;print(json.load(sys.stdin)['roomID'])"
}

TCPURL=$1
EXEC=$2

for i in $(seq 3 2 $#) ; do
	DIR=${!i}
	i=$(($i + 1))
	CSV=${!i}
	
	ROOM=$(get_new_room $(date "+Room=%Y%m%d%H%M%S"))
	
	echo "Starting new room. roomID=$ROOM"
	python $TESTSCRIPT $TCPURL $DIR $EXEC $ROOM $CSV &
	sleep 30 # Sec
done
