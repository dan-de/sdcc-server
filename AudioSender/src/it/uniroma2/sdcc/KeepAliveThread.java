package it.uniroma2.sdcc;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class KeepAliveThread extends Thread {

	Socket sock;
	int timeout;
	
	public KeepAliveThread(Socket sock, int timeout) {
		this.sock = sock;
		this.timeout = timeout;
	}
	
	@Override
	public void run() {
		
		System.out.println("Keep Alive Thread started");
		
		try {
			sock.setSoTimeout(timeout);
			
			while ( sock.getInputStream().read() != -1 )
				;
			
			System.out.println("Server close connection");
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			System.out.println("Server timeout");
			try {
				sock.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
