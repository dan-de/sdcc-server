package it.uniroma2.sdcc;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class NetworkThread extends Thread {
	
	public static final String TAG = "NetworkThread";
	public static final int CONNECT_SLEEP_SECONDS = 5;
	
	public static volatile boolean networkGood;

	private Socket sock;
	
	private BlockingQueue<AudioChunk> queue;
	
	int totalBytesSent;

	private AudioChunk credentials;

	private String host;

	private boolean throttled;

	public NetworkThread(String host, AudioChunk cred, boolean throttle) {
		
		queue = new ArrayBlockingQueue<AudioChunk>(1024, true);
		
		this.host = host;

		credentials = cred;

		networkGood = false;

		throttled = throttle;
	}

	private void tryConnect() throws IOException {

		sock = new Socket();

		sock.connect(new InetSocketAddress(host, 9999));

		System.out.println("Connected.");

		if (credentials != null)
			sock.getOutputStream().write(credentials.getBuffer(), 0, credentials.getSize());
	}
	
	private void sendOut(final AudioChunk job) {
		try
		{
			sock.getOutputStream().write(job.getBuffer(), 0, job.getSize());
			
			sock.getOutputStream().flush();
			
			// System.out.println("Sent " + job.getSize() + " bytes at " + System.currentTimeMillis());

			totalBytesSent += job.getSize();
		}
		catch (IOException e) {
			networkGood = false;
		}
	}

	@Override
	public void run() {
		
		System.out.println("Thread start");
				
		AudioChunk job = null;
		
		totalBytesSent = 0;
		
		while (true) {
			try
			{
				if (!networkGood) {
					tryConnect();
					queue.clear();
					networkGood = true;
					Main.lock.lock();
					Main.networkRestored.signal();
					Main.lock.unlock();
					new KeepAliveThread(sock, 10000).start();
				}

				job = queue.take();
			} catch (IOException e) {
				System.err.println("Unable to connect, retrying in " + CONNECT_SLEEP_SECONDS + " sec...");
				try {
					Thread.sleep(CONNECT_SLEEP_SECONDS * 1000);
					continue;
				} catch (InterruptedException e1) {
					System.out.println("Network Loop interrupted (while sleeping)");
					break;
				}
			}
			catch (InterruptedException e) {
				System.out.println("Network Loop interrupted");
				break;
			}
			
			sendOut(job);

			/*if (throttled) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.out.println("Network Loop interrupted (while throttling)");
				}
			}*/
		}
		
		System.out.println("Flush queue...");
		
		while (!queue.isEmpty()) {
			job = queue.poll();
			
			if (networkGood) {
				sendOut(job);

				if (throttled) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						System.out.println("Network Flushing interrupted (while throttling)");
					}
				}
			}
		}
		
		System.out.println("Elements in queue: " + queue.size());
		System.out.println("TotalBytesSent: " + totalBytesSent);
		System.out.println("Thread exit");
	}

	
	public void disconnectStop() {
		
		interrupt();
		
		try {
			join();
			sock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public Queue<AudioChunk> getInputQueue() {
		return queue;
	}
}
