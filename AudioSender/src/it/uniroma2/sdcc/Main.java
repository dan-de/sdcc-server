package it.uniroma2.sdcc;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Main {
	
	public static final String CRED_KEY_USERNAME = "username";
	public static final String CRED_KEY_TOKEN = "token";
	public static final String CRED_KEY_ROOMID = "roomId";
	
	private static int buffer_size = 8192;

	public static Lock lock = new ReentrantLock();
	public static Condition networkRestored = lock.newCondition();

	private static String prepareJSON(String username, String token, String roomID)
	{
		String userJson = String.format("\"%s\":\"%s\"", CRED_KEY_USERNAME, username);
		String tokenJson = String.format("\"%s\":\"%s\"", CRED_KEY_TOKEN, token);
		String roomJson = String.format("\"%s\":\"%s\"", CRED_KEY_ROOMID, roomID);
		
		return String.format("{%s,%s,%s}", userJson, tokenJson, roomJson);
	}
	
	private static AudioChunk prepareCredentials(String username, String token, String roomID)
	{
		ByteArrayOutputStream outputStream;
		DataOutputStream dataStream;

		HashMap<String, String> m = new HashMap<>();

		m.put(CRED_KEY_USERNAME, username);
		m.put(CRED_KEY_TOKEN, token);
		m.put(CRED_KEY_ROOMID, roomID);

		// JSONObject jsonCred = new JSONObject(m);

		String json = prepareJSON(username, token, roomID); // jsonCred.toString();
		
		byte[] json_bytes = json.getBytes(Charset.forName("UTF-8"));

		System.out.println("JSON: " + json);

		outputStream = new ByteArrayOutputStream(4 + json.length());
		dataStream = new DataOutputStream(outputStream);

		try {
			dataStream.writeInt(json_bytes.length);
			dataStream.write(json_bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new AudioChunk(outputStream.toByteArray(), 4+json.length());
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		
		AudioChunk cred;
		BufferedInputStream istream;
		CommandLine cmd;
		NetworkThread net;
		Queue<AudioChunk> netQueue;
		HelpFormatter hf = new HelpFormatter();
		
		String host = System.getenv("SDCC_TCP_SERVER");
		
		if (host == null) {
			System.err.println("Please set SDCC_TCP_SERVER environment variable");
			return;
		}
		
		System.out.println("Read host from env: " + host);
		
		Options options = new Options();
		
		options.addOption("i", true, "Input file");
		options.addOption("nc", false, "Disable sending credentials");
		options.addOption("u", true, "User name");
		options.addOption("t", true, "Token");
		options.addOption("r", true, "RoomId");
		options.addOption("b", true, "Output bitrate");
		
		CommandLineParser parser = new BasicParser();
		
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			
			hf.printHelp("sender.jar", options);
			
			return;
		}
		
		String filename = cmd.getOptionValue("i");
		String user = cmd.getOptionValue("u");
		String token = cmd.getOptionValue("t");
		String roomId = cmd.getOptionValue("r");
		boolean no_credentials = cmd.hasOption("nc");
		String bitrateStr = cmd.getOptionValue("b");
		boolean is_throttled = (bitrateStr != null);

		if (filename == null) {
			System.err.println("Missing filename (mandatory).");
			hf.printHelp("sender.jar", options);
			return;
		}
		
		if (!no_credentials && (user == null || token == null || roomId == null)) {
			hf.printHelp("sender.jar", options);
			return;
		}
		
		System.out.println("Filename: " + filename);

		if (no_credentials) {
			cred = null;
		}
		else {
			cred = prepareCredentials(user, token, roomId);
		}
		
		if (is_throttled) {
			int bitrate = Integer.parseInt(bitrateStr);

			// Compute buffer size

			buffer_size = (int) ((bitrate * 1000)/8.0);
		}

		net = new NetworkThread(host, cred, is_throttled);
		
		net.start();
				
		try {
			istream = new BufferedInputStream(new FileInputStream(filename));
		} catch (FileNotFoundException e) {
			System.err.println("File not found: " + filename);
			return;
		}
		
		byte[] buffer = new byte[buffer_size];
		
		int rd;
		
		netQueue = net.getInputQueue();
		
		int byteOffered = 0;

		// Wait for good network before read file
		while (!NetworkThread.networkGood);

		while ((rd=istream.read(buffer, 0, buffer_size)) > 0) {

			if (NetworkThread.networkGood) {
				while (!netQueue.offer(new AudioChunk(buffer, rd)));

				byteOffered += rd;

				buffer = new byte[buffer_size];
			}

			if (is_throttled) {
				lock.lock();
				try {
					networkRestored.await(1, TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					System.out.println("Main Loop interrupted (while throttling)");
				} finally {
					lock.unlock();
				}
			}
		}
		
		istream.close();
		
		System.out.println("EOF, read " + byteOffered + " bytes.");
		
		net.disconnectStop();
	}

}
