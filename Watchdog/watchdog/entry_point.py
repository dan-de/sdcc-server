import datetime
import time

from aws_services import getExpired, updateTimestamp
from threads import wakeupFilteringServer, wakeupOutputServer, healthCheckGuard

def epoch_time_ms():
	epoch = datetime.datetime.utcfromtimestamp(0)
	now = datetime.datetime.today()
	now = now - epoch
	return int(now.total_seconds() * 1000)

def get_threshold_time():
	return epoch_time_ms() - 15000 # 15 sec past time

def get_new_timestamp():
	return epoch_time_ms() + 15000



print('Server started.')

healthCheckGuard()

while True:
	thr = get_threshold_time()
	tuples = getExpired(thr)
	
	try:
		for t in tuples:
			
			roomID = t['Room Name']
			saveUrl = t['Url Saving']
			
			if t['Server Type'] == 1:
				serviceUrl = t['Url Service']
				print('Waking up filtering server for ' + roomID)
				wakeupFilteringServer(serviceUrl, saveUrl, roomID)
				if not updateTimestamp(t, get_new_timestamp()):
					print('Failed updating timestamp for filtering server')
			elif t['Server Type'] == 2:
				print('Waking up saving server for ' + roomID)
				wakeupOutputServer(saveUrl, roomID)
				if not updateTimestamp(t, get_new_timestamp()):
					print('Failed updating timestamp for saving server')
			else:
				print('Invalid server type read in the DB')
	except:
		print('ERROR: Something wrong in DB table!')
	
	time.sleep(10) # Secs
