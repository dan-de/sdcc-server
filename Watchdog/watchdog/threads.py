import socket
import sys
import threading

from local_settings import FILTERING_WAKEUP_URL, OUTPUT_WAKEUP_URL

def wakeupFilteringServerThread(serviceURL, outputURL, roomID):
	message = '{"Service": "' + serviceURL + '", "Output": "' + outputURL + '", "roomID": "' + roomID + '"}'
	print('wakeupFilteringServer: Sending wakeup message for room ' + roomID)
	
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # TCP/IP
	sock.settimeout(5)
	
	try:
		sock.connect(FILTERING_WAKEUP_URL)
		sock.sendall(message.encode())
	except socket.timeout:
		print('wakeupFilteringServer: Timed out')
	finally:
		sock.close()
	
	print('wakeupFilteringServer: Done')

def wakeupOutputServerThread(outputURL, roomID):
	message = '{"Output": "' + outputURL + '", "roomID": "' + roomID + '"}'
	print('wakeupOutputServer: Sending wakeup message for room ' + roomID)
	
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # TCP/IP
	sock.settimeout(5)
	
	try:
		sock.connect(OUTPUT_WAKEUP_URL)
		sock.sendall(message.encode())
	except socket.timeout:
		print('wakeupOutputServer: Timed out')
	finally:
		sock.close()
	
	print('wakeupOutputServer: Done')

def wakeupFilteringServer(serviceURL, outputURL, roomID):
	try:
		print('Starting new threads for wakingup...')
		t = threading.Thread(target=wakeupFilteringServerThread, args=(serviceURL, outputURL, roomID))
		t.start()
		print('Start completed.')
	except:
		print('Error while creating thread!')

def wakeupOutputServer(outputURL, roomID):
	try:
		print('Starting new threads for wakingup...')
		t = threading.Thread(target=wakeupOutputServerThread, args=(outputURL, roomID))
		t.start()
		print('Start completed.')
	except:
		print('Error while creating thread!')



# Load balancer health check

def healthCheckGuardThread():
	server_address = ('0.0.0.0', 8086)
	
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.bind(server_address)
	sock.listen(1)
	
	while True:
		connection, client_address = sock.accept()
		#print(client_address)
		try:
			data = connection.recv(16)
			while data:
				print('healthCheckGuard: data received = ' + data.decode('utf-8'))
				data = connection.recv(16)
		except Exception as e:
			print('healthCheckGuard: ' + str(e))
		finally:
			#print('healthCheckGuard: disconnected')
			connection.close()

def healthCheckGuard():
	try:
		print('Starting new thread for health check guard')
		t = threading.Thread(target=healthCheckGuardThread)
		t.start()
		print('Start completed.')
	except:
		print('Error while creating thread!')

