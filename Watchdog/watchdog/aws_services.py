from boto.dynamodb2 import connect_to_region
from boto.dynamodb2.table import Table
from boto.dynamodb2.results import ResultSet

from local_settings import AWS_REGION, DYNAMODB_TABLE

conn = connect_to_region(AWS_REGION)
watchdog = Table(DYNAMODB_TABLE, connection=conn)

def getExpired(pastTime):
	try:
		results = watchdog.scan(Timestamp__lt=pastTime, limit=10)
	except:
		print('ERROR: Unable to find DB table!!')
		return []
	
	return results

def updateTimestamp(entry, timestamp):
	entry['Timestamp'] = timestamp
	
	return entry.save(overwrite=True)
	
