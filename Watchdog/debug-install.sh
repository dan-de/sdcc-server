#!/bin/bash

if [ "$(id -u)" != "0" ] ; then
	echo "You must be root to execute this script" 1>&2
	exit 2
fi

apt-get update
apt-get -f -y dist-upgrade
apt-get -y install screen git vim htop python-pip python-virtualenv

cd /opt

git clone https://MrModd@bitbucket.org/dan-de/sdcc-server.git

cp sdcc-server/Watchdog/watchdog/local_settings.py.example sdcc-server/Watchdog/watchdog/local_settings.py

vim sdcc-server/Watchdog/watchdog/local_settings.py

cd sdcc-server/Watchdog/

./create-environment.sh

FILE="/etc/init.d/watchdog.sh"
echo "#!/bin/bash" > $FILE
echo "### BEGIN INIT INFO" >> $FILE
echo "# Provides:				watchdog.sh" >> $FILE
echo "# Required-Start:		mountkernfs \$local_fs" >> $FILE
echo "# Required-Stop:		\$local_fs" >> $FILE
echo "# Default-Start:		2 3 4 5" >> $FILE
echo "# Default-Stop:			0 1 6" >> $FILE
echo "# Short-Description:	Start watchdog service" >> $FILE
echo "### END INIT INFO" >> $FILE
echo "" >> $FILE
echo "USERNAME=\"ubuntu\"" >> $FILE
echo "SCRIPT=/opt/sdcc-server/Watchdog/run-server.sh" >> $FILE
echo "EXECUTABLE=\"entry_point.py\"" >> $FILE
echo "" >> $FILE
echo "sstart() {" >> $FILE
echo "	su - \$USERNAME -c \"screen -h 1024 -dmS watchdog \$SCRIPT\"" >> $FILE
echo "	sleep 5" >> $FILE
echo "	echo \"Done.\"" >> $FILE
echo "}" >> $FILE
echo "" >> $FILE
echo "sstop() {" >> $FILE
echo "	kill \$(ps ax | grep \$EXECUTABLE | grep -v grep | awk '{print \$1}')" >> $FILE
echo "	echo \"Done.\"" >> $FILE
echo "}" >> $FILE
echo "" >> $FILE
echo "case \"\$1\" in" >> $FILE
echo "	start)" >> $FILE
echo "		echo \"Starting...\"" >> $FILE
echo "		sstart" >> $FILE
echo "		;;" >> $FILE
echo "	stop)" >> $FILE
echo "		echo \"Stopping...\"" >> $FILE
echo "		sstop" >> $FILE
echo "		;;" >> $FILE
echo "	*)" >> $FILE
echo "		echo \"Usage: \$0 { start | stop }\"" >> $FILE
echo "		;;" >> $FILE
echo "esac" >> $FILE
echo "" >> $FILE
echo "exit 0" >> $FILE
echo "" >> $FILE

chmod +x $FILE

update-rc.d watchdog.sh defaults

service watchdog.sh start

exit 0
