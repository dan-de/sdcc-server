#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $DIR/environment 2>/dev/null
if [ $? != "0" ] ; then
	echo -e "Cannot import environment path" >&2
	exit 1
fi



##########################
# Checking configuration #
##########################

echo -e "Checking configuration...\n" >&2

echo -n "Checking python3... "
if [ ! `command -v python3 2>/dev/null` ] ; then
	echo -e "FAILED!\n\nPlease install python3 and retry."
	exit 1
fi
echo "OK!"

echo -n "Checking virtualenv... "
if [ ! `command -v virtualenv 2>/dev/null` ] ; then
	echo -e "FAILED!\n\nPlease install virtualenv and retry."
	exit 1
fi
echo "OK!"

echo -n "Checking pip... "
if [ ! `command -v pip 2>/dev/null` ] ; then
	echo -e "FAILED!\n\nPlease install pip and retry."
	exit 1
fi
echo "OK!"

echo -n "Checking environment path... "
if [[ $VIRT_ENV =~ ^.*[[:space:]].*$ ]] ; then
	echo -e "FAILED!\n\nENVIRONMENT path must not contain spaces" >&2
	exit 1
fi
echo "OK!"



echo -ne "\nInstalling virtual environment... "

if [ `virtualenv -p python3 $VIRT_ENV >/dev/null` ] ; then
	echo -e "FAILED!\n\nCannot install virtual environment.\nIs the directory writable?" >&2
	exit 1
fi
echo "OK!"



source $VIRT_ENV/bin/activate
if [ $? != "0" ] ; then
	echo -e "Cannot access virtual environment" >&2
	exit 1
fi



#######################
# Installing packages #
#######################

# Requirements for Boto
echo -ne "Installing Boto (AWS SDK)... "
pip install boto >/dev/null
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Boto." >&2
	exit 1
fi
echo "OK!"

echo -e "\nEND"

exit 0
