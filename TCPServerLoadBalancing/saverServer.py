#!/usr/bin/python
import socket
import sys
import json
import subprocess

program = "/opt/SaverServer.jar"
port = 8008

def startNewServer(jsonString):
    try:
        decodedJson = json.loads(jsonString)
        roomName = decodedJson['roomID']
        urlSaveQueue = decodedJson['Output']
        print str(jsonString)
        """ Call a new Java Filter process with the Json Fields""" 
        subprocess.call("java -server -d64 -jar "+program+" "+roomName+" "+urlSaveQueue+" &", shell=True) 
        print >> sys.stdout, "All done"
    except Exception as e:
        pass
        #print >> sys.stderr, "Error: not a valid json"
    finally:
        pass
    

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('0.0.0.0', port)
print >> sys.stdout, 'starting up on %s port %s' % server_address 
print >> sys.stdout
sock.bind(server_address)

sock.listen(1)

while True:
    print >> sys.stdout, 'waiting for a connection'
    connection, client_address = sock.accept()
    
    jsonString = ""
    try:
        print >> sys.stdout, 'connection from', client_address
        while True:
            data = connection.recv(16)
            #print >> sys.stdout, 'received "%s"' % data
            if data:
                jsonString=jsonString+data
            else:
                #print >> sys.stdout, 'no more data from', client_address
                break
        if len(jsonString)>0:
            print jsonString
            startNewServer(jsonString)
        else:
            pass
            #print >> sys.stderr, "Error: json length 0. Closing..."
    except Exception as e:
        pass
        #print >> sys.stderr, "Error in connection. Closing..."
    finally:
        connection.close() 
