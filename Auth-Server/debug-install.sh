#!/bin/bash

if [ "$(id -u)" != "0" ] ; then
	echo "You must be root to execute this script" 1>&2
	exit 2
fi

apt-get update
apt-get -f -y dist-upgrade

apt-get install -y git vim htop python-pip python-virtualenv \
apache2 libapache2-mod-wsgi-py3

cd /var/www

mkdir static
mkdir media

git clone https://MrModd@bitbucket.org/dan-de/sdcc-server.git

cp sdcc-server/Auth-Server/auth_server/auth_server/local_settings.py.example sdcc-server/Auth-Server/auth_server/auth_server/local_settings.py

vim sdcc-server/Auth-Server/auth_server/auth_server/local_settings.py

cd sdcc-server/Auth-Server/

./create-environment.sh
./update-static.sh

rm -f /etc/apache2/sites-enabled/000-default.conf
FILE="/etc/apache2/sites-enabled/000-django.conf"
echo "<VirtualHost *:80>" > $FILE
echo "	WSGIScriptAlias / /var/www/sdcc-server/Auth-Server/auth_server/auth_server/wsgi.py" >> $FILE
echo "	WSGIDaemonProcess localhost python-path=/var/www/sdcc-server/Auth-Server/auth_server:/var/www/sdcc-server/Auth-Server/virtual/lib/python3.4/site-packages" >> $FILE
echo "	WSGIProcessGroup localhost" >> $FILE
echo "" >> $FILE
echo "	WSGIPassAuthorization On" >> $FILE

echo "	Alias /static/ /var/www/static/" >> $FILE

echo "	<Directory /var/www/static>" >> $FILE
echo "		Require all granted" >> $FILE
echo "	</Directory>" >> $FILE
echo "" >> $FILE
echo "	Alias /media/ /var/www/media/" >> $FILE
echo "" >> $FILE
echo "	<Directory /var/www/media>" >> $FILE
echo "		Require all granted" >> $FILE
echo "	</Directory>" >> $FILE
echo "" >> $FILE
echo "	<Directory /var/www/sdcc-server/Auth-Server/auth_server/auth_server>" >> $FILE
echo "		<Files wsgi.py>" >> $FILE
echo "			Require all granted" >> $FILE
echo "		</Files>" >> $FILE
echo "	</Directory>" >> $FILE
echo "</VirtualHost>" >> $FILE

chown -R www-data:www-data /var/www

service apache2 stop
sleep 1
service apache2 start

exit 0
