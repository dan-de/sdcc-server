from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls import include

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    
    # User management and main web application
    url(r'^', include('users.urls', namespace='users')),
    url(r'^rest/', include('recordings.urls', namespace='recordings')),
    
    # REST auth system
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)
