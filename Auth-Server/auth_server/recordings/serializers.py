from rest_framework import serializers
from recordings import models

class RoomSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.RoomListRESTObject
		fields = ('roomID', 'roomName', 'participant', 'distance')
		read_only_fields = ('roomID', 'roomName', 'participant', 'distance')

class RoomCreationSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.RoomCreationRESTObject
		fields = ('roomName', 'latitude', 'longitude')

class RoomDetailSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.Room
		fields = ('roomID', 'roomName', 'start_recording', 'stop_recording', 'latitude', 'longitude', 'stream_url', 'service_queue_url', 'output_queue_url', 'on_recording')
		read_only_fields = ('roomID', 'start_recording', 'latitude', 'longitude', 'stream_url', 'service_queue_url', 'output_queue_url')

class RoomDetailForClientSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.Room
		fields = ('roomID', 'roomName', 'start_recording', 'latitude', 'longitude', 'stream_url', 'on_recording')
		read_only_fields = ('roomID', 'roomName', 'start_recording', 'latitude', 'longitude', 'stream_url', 'on_recording')

class RoomUserSerializer(serializers.ModelSerializer):
	room = serializers.SlugRelatedField(read_only=True, slug_field='roomID')
	user = serializers.SlugRelatedField(read_only=True, slug_field='username')
	
	class Meta:
		model = models.UserRoomUrl
		fields = ('room', 'user', 'user_queue_url', 'creation')
		read_only_fields = ('room', 'user', 'user_queue_url', 'creation')

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.UserRESTObject
		fields = ('username',)



class RecordingSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.Recording
		fields = ('recordingID', 'recordingName', 'start_recording', 'stop_recording', 'length', 'dimension', 'latitude', 'longitude', 'download_url', 'available')
		read_only_fields = ('users',)
