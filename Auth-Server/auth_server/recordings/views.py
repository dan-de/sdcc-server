from django.shortcuts import render, get_object_or_404
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import permission_required
from django.utils.crypto import get_random_string
from django.core.mail import send_mail, send_mass_mail

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions

import threading

from geopy.distance import vincenty

from recordings import models, serializers
from .aws_services import *

from django.conf import settings

from syslog import syslog as print



# Just for debug purpose. They should be set to False when in production!
DISABLE_AWS_COMMANDS = False
DISABLE_TCP_MESSAGES = False
DISABLE_EMAIL = False



RADIUS = 1000 # meters

def generateSerial():
	chars = '0123456789abcdefghijklmnopqrstuvwxyz'
	return get_random_string(16, chars)

def emailSender(recordingObj):
	if not recordingObj.available:
		return None
	
	print('Sending notification emails to users')
	
	# Fetch all user email
	emails = get_user_model().objects.filter(recordings=recordingObj).values_list('email', flat=True)
	print('Users: ' + str(emails))
	
	# Send multiple email with single connection
	tuples = []
	for user in get_user_model().objects.filter(recordings=recordingObj):
		subject = 'Your recording is ready!'
		from_email = 'No Reply <' + settings.EMAIL_HOST_USER + '>'
	
		message = 'Dear ' + user.username + ',\n'
		message += 'you participated recording room ' + recordingObj.recordingName + ' with ID ' + recordingObj.recordingID + '\n'
		message += 'and now your file is ready.\n\n'
		message += 'Please check it out on your Android application of use the following link to access it directly:\n'
		message += recordingObj.download_url
		
		recipient = [user.email,]
		
		tuples.append((subject, message, from_email, recipient))
	send_mass_mail(tuples, fail_silently=settings.DEBUG)
	
	# Send every email in a different connection
	"""for user in get_user_model().objects.filter(recordings=recordingObj):
		subject = 'Your recording is ready!'
		from_email = 'No Reply <' + settings.EMAIL_HOST_USER + '>'
		
		message = 'Dear ' + user.username + ',\n'
		message += 'you participated recording room ' + recordingObj.recordingName + ' with ID ' + recordingObj.recordingID + '\n'
		message += 'And now your file is ready.\n\n'
		message += 'Please check it out on your Android application of use the following link to access directly:\n'
		message += recordingObj.download_url + '\n\n'
		
		html_message = '<p>Dear ' + user.username + ',<br />'
		html_message += 'you participated recording room ' + recordingObj.recordingName + ' with ID ' + recordingObj.recordingID + '<br />'
		html_message += 'And now your file is ready.</p>'
		html_message += '<p>Please check it out on your Android application of use the following link to access directly:<br />'
		html_message += '<a href="' + recordingObj.download_url + '">' + recordingObj.download_url + '</a></p>'
		
		mail = send_mail(subject, message, from_email, [user.email], fail_silently=settings.DEBUG, html_message=html_message)"""
	
	print('Emails sent.')



# Rooms

class RoomList(APIView):
	permission_classes = (permissions.IsAuthenticated,)
	
	# This should be called from client App
	def get(self, request, format=None):
		# Print all rooms on recording in the given range
		
		# Range within look for active rooms
		# eg. ?lat=42.000&lon=12.000&radius=1000 will look for
		# all active rooms in a radius of 1km centered in
		# 42.000N 12.000E
		latitude = request.GET.get('lat', '')
		longitude = request.GET.get('lon', '')
		radius = request.GET.get('radius', RADIUS)
		
		radius = int(radius)
		
		roomsObj = models.Room.objects.filter(on_recording=True)
		
		if latitude and longitude:
			latitude = float(latitude)
			longitude = float(longitude)
			
			userPosition = (latitude, longitude)
		
		serializedRooms = []
		for r in roomsObj:
			if latitude and longitude:
				roomPosition = (r.latitude, r.longitude)
				distance = vincenty(roomPosition, userPosition).meters
				if distance <= radius:
					s = models.RoomListRESTObject()
					s.roomID = r.roomID
					s.roomName = r.roomName
					s.participant = len(models.UserRoomUrl.objects.filter(room=r))
					s.distance = distance
					serializedRooms.append(s)
			else:
				s = models.RoomListRESTObject()
				s.roomID = r.roomID
				s.roomName = r.roomName
				s.participant = len(models.UserRoomUrl.objects.filter(room=r))
				s.distance = 0
				serializedRooms.append(s)
		
		serializer = serializers.RoomSerializer(serializedRooms, many=True)
		return Response(serializer.data)
	
	# This should be called from client App
	def post(self, request, format=None):
		# Create a new room
		
		ser = serializers.RoomCreationSerializer(data=request.DATA)
		if ser.is_valid():
			roomID = generateSerial()
			while models.Room.objects.filter(roomID=roomID).exists() or models.Recording.objects.filter(recordingID=roomID).exists():
				roomID = generateSerial()
			
			roomObj = models.Room()
			roomObj.roomID = roomID
			roomObj.roomName = ser.validated_data.get('roomName', '') # This field is optional
			roomObj.latitude = ser.validated_data['latitude']
			roomObj.longitude = ser.validated_data['longitude']
			
			if not DISABLE_AWS_COMMANDS:
				roomObj.stream_url = getStreamUrl(roomID)
				roomObj.service_queue_url = getServiceUrl(roomID)
				roomObj.output_queue_url = getOutputUrl(roomID)
			else:
				# Doesn't check for already generated serials (it's just for debug)
				roomObj.stream_url = generateSerial()
				roomObj.service_queue_url = generateSerial()
				roomObj.output_queue_url = generateSerial()
				
			roomObj.on_recording = True
			roomObj.save()
			
			if not DISABLE_TCP_MESSAGES:
				try:
					print('Starting wakeup thread for filtering server...')
					t = threading.Thread(target=wakeupFilteringServer, args=(roomObj.service_queue_url, roomObj.output_queue_url, roomID))
					t.start()
					print('Start completed.')
				except:
					print('Error while creating thread!')
			
			# Create an object for the final S3 file too. It will be updated later with
			# more informations
			recordingObj = models.Recording()
			recordingObj.recordingID = roomID
			recordingObj.recordingName = roomObj.roomName
			recordingObj.start_recording = roomObj.start_recording
			recordingObj.latitude = roomObj.latitude
			recordingObj.longitude = roomObj.longitude
			recordingObj.save()
			
			print('New room created with ID ' + roomID)
			
			serializer = serializers.RoomDetailForClientSerializer(roomObj)
			return Response(serializer.data, status=status.HTTP_201_CREATED)
			
		return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)



class RoomDetail(APIView):
	permission_classes = (permissions.IsAuthenticated,)

	# This should be called from client App after have selected the room to join
	def get(self, request, roomID, format=None):
		# Print room details
		
		try:
			room = models.Room.objects.get(roomID=roomID)
		except models.Room.DoesNotExist:
			content = {'Error': 'Room not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		if request.user.has_perm('recordings.change_room'):
			serializer = serializers.RoomDetailSerializer(room)
		else:
			serializer = serializers.RoomDetailForClientSerializer(room)
		return Response(serializer.data)
	
	# This should be called exclusively from server in order to set an
	# end time for the recording and set on_recording to False
	def put(self, request, roomID, format=None):
		# Update room details
		
		# Check permission
		if not request.user.has_perm('recordings.change_room'):
			return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
		
		try:
			room = models.Room.objects.get(roomID=roomID)
		except models.Room.DoesNotExist:
			content = {'Error': 'Room not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		ser = serializers.RoomDetailSerializer(room, data=request.DATA, partial=True)
		
		if ser.is_valid():
			ser.save()
			print('Room ' + roomID + ' updated')
			return Response(ser.data, status=status.HTTP_200_OK)
		
		print('Room ' + roomID + ': update failed!')
		return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
	
	# This should be called exclusively from server after the recording
	# has been processed and final audio file has been put on S3
	def delete(self, request, roomID, format=None):
		# Remove a room
		
		# Check permission
		if not request.user.has_perm('recordings.delete_room'):
			return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
		
		try:
			room = models.Room.objects.get(roomID=roomID)
		except models.Room.DoesNotExist:
			content = {'Error': 'Room not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		if room.on_recording:
			content = {'Error': 'Recording on-air'}
			return Response(content, status=status.HTTP_403_FORBIDDEN)
		
		output_queue = room.output_queue_url
		
		# TODO: probably next 2 lines must be deleted
		#deleteQueueByURL(room.service_queue_url)
		#deleteQueueByURL(room.output_queue_url)
		room.delete()
		
		print('Room with ID ' + roomID + ' removed')
		
		# If a user creates a room and noone start recording in it
		# this room has no audio track and his related Recording object
		# doesn't have (and will never have) an S3 file linked to it.
		# So, if this room is deleted check if the related recording has
		# users, otherwise delete that object too.
		recording = models.Recording.objects.get(recordingID=roomID)
		# Should never fail getting the object because it is created at the same time
		# of the creation of the room
		if not recording.users.all():
			if not DISABLE_AWS_COMMANDS:
				deleteS3File(recording.download_url)
			recording.delete()
			print('No users entered in Room ' + roomID + ', removing recording object too')
		else:
			if not DISABLE_TCP_MESSAGES:
				try:
					print('Starting wakeup thread for saving server...')
					t = threading.Thread(target=wakeupOutputServer, args=(output_queue, roomID))
					t.start()
					print('Start completed.')
				except:
					print('Error while creating thread!')
		
		return Response(status=status.HTTP_204_NO_CONTENT)



class RoomUsersList(APIView):
	permissions_classes = (permissions.IsAuthenticated,)
	
	# This method is just for debug and should be restricted from client access
	def get(self, request, roomID, format=None):
		# Print all user in this room
		
		# Check permission
		if not request.user.is_superuser:
			return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
		
		roomUsers = models.UserRoomUrl.objects.filter(room__roomID=roomID)
		serializer = serializers.RoomUserSerializer(roomUsers, many=True)
		return Response(serializer.data)
	
	# This should be called exclusively from server when the client ensablish
	# a TCP connection and start sending audio stream
	def post(self, request, roomID, format=None):
		# Add user to this room
		
		# Check permission
		if not request.user.has_perm('recordings.add_userroomurl'):
			return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
		
		try:
			room = models.Room.objects.get(roomID=roomID)
		except models.Room.DoesNotExist:
			content = {'Error': 'Room not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		recording = models.Recording.objects.get(recordingID=roomID)
		# Should never fail getting the object because it is created at the same time
		# of the creation of the room
		
		ser = serializers.UserSerializer(data=request.DATA)
		
		if ser.is_valid():
			username = ser.validated_data['username']
			try:
				user = get_user_model().objects.get(username=username)
			except get_user_model().DoesNotExist:
				content = {'Error': 'User not found'}
				return Response(content, status=status.HTTP_404_NOT_FOUND)
			
			# If user is already present in another room refuse him
			if models.UserRoomUrl.objects.filter(user=user).exclude(room=room).exists():
				content = {'Error': 'User already in another room'}
				return Response(content, status=status.HTTP_409_CONFLICT)
			
			if not DISABLE_AWS_COMMANDS:
				rand = generateSerial()
				queueUrl = getUserUrl(roomID, user.username, rand)
				while models.UserRoomUrl.objects.filter(user_queue_url=queueUrl).exists():
					rand = generateSerial()
					queueUrl = getUserUrl(roomID, user.username, rand)
					# If the queue already exists AWS library fails silently and returns the preexistent queue
			else:
				# Doesn't check for already generated serials (it's just for debug)
				queueUrl = generateSerial()
			
			userRoomUrl = models.UserRoomUrl()
			userRoomUrl.user_queue_url = queueUrl
			userRoomUrl.room = room
			userRoomUrl.user = user
			userRoomUrl.save()
			
			if not DISABLE_AWS_COMMANDS:
				sendJoinMessage(room.service_queue_url, username, queueUrl)
			
			recording.users.add(user)
			recording.save()
			
			print('User ' + user.username + ' has joined Room ' + roomID)
			
			serializer = serializers.RoomUserSerializer(userRoomUrl)
			# Response contains the user queue URL too!!
			return Response(serializer.data)
		
		return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)



class RoomUsersDetail(APIView):
	permissions_classes = (permissions.IsAuthenticated,)
	
	# This should be called exclusively from server and "could" (or could not) be
	# useful to get the queue URL of the client. Maybe this method will never be used.
	def get(self, request, roomID, username, format=None):
		# Get user queue URL for this room
		
		# Check permission
		if not request.user.is_superuser:
			return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
		
		url = request.GET.get('url', '')
		if url:
			roomUserObj = models.UserRoomUrl.objects.filter(room__roomID=roomID, user__username=username, user_queue_url=url)
		else:
			roomUserObj = models.UserRoomUrl.objects.filter(room__roomID=roomID, user__username=username)
		
		if not roomUserObj:
			if url:
				content = {'Error': 'Queue URL not found for the specific user'}
			else:
				content = {'Error': 'User not found in this room'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		serializer = serializers.RoomUserSerializer(roomUserObj, many=True)
		return Response(serializer.data)
	
	# This should be called exclusively from server when a TCP connection ends
	def delete(self, request, roomID, username, format=None):
		# Remove user from the room
		
		# Check permission
		if not request.user.has_perm('recordings.delete_userroomurl'):
			return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
		
		url = request.GET.get('url', '')
		if url:
			roomUserObj = models.UserRoomUrl.objects.filter(room__roomID=roomID, user__username=username, user_queue_url=url)
		else:
			roomUserObj = models.UserRoomUrl.objects.filter(room__roomID=roomID, user__username=username)
		if not roomUserObj:
			content = {'Error': 'User not found in this room or room not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		# TODO: probably next 2 lines must be deleted
		#roomUserObj = roomUserObj[0]
		#deleteQueueByURL(roomUserObj.user_queue_url)
		roomUserObj.delete() # If URL not given, this will delete ALL objects related to the user in the room
		
		print('User ' + username + ' leaved Room ' + roomID)
		
		if len(models.UserRoomUrl.objects.filter(room__roomID=roomID)) == 0:
			print('No more users in the room ' + roomID + ' setting on_recording to False')
			try:
				room = models.Room.objects.get(roomID=roomID)
				room.on_recording = False
				room.save()
			except models.Room.DoesNotExist:
				print('ERROR: roomID not found after deleting RoomUserUrl object')
		
		return Response(status=status.HTTP_204_NO_CONTENT)



# Recordings

class RecordingList(APIView):
	permission_classes = (permissions.IsAuthenticated,)
	
	# This should be called by client App for the personal recordings listing
	def get(self, request, format=None):
		# Show current authenticated user recordings list,
		# but show just those available (that has an S3 file).
		# Recordings not available are those on hold to receive
		# the processed file from the filtering cluster. Users
		# should wait that the file is processed before list it
		# on their client App.
		
		#userRecords = request.user.recordings.all()
		userRecords = models.Recording.objects.filter(users=request.user).exclude(available=False)
		
		serializer = serializers.RecordingSerializer(userRecords, many=True)
		return Response(serializer.data)
	
	# This should be called exclusively by the server when a processed file has been put
	# on S3
	"""def post(self, request, format=None):
		# Add a recording
		
		# Check permission
		if not request.user.has_perm('recordings.add_recording'):
			return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
		
		ser = serializers.RecordingSerializer(data=request.DATA)
		
		if ser.is_valid():
			ser.save()
			return Response(ser.data)
		
		return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)"""
	# The creation of the Recording object moved to the POST method of RoomList



class RecordingDetail(APIView):
	permission_classes = (permissions.IsAuthenticated,)
	
	# This should be called by client App when user requires the recording details or
	# want to download it on the phone
	def get(self, request, recordingID, format=None):
		# Show recording details
		
		try:
			if request.user.is_superuser:
				recordingObj = models.Recording.objects.get(recordingID=recordingID) # The power of the admin!
			else:
				# This should not be fetched if it is not available yet or user have no permissions to access it
				recordingObj = models.Recording.objects.filter(users=request.user).get(recordingID=recordingID, available=True)
		except models.Recording.DoesNotExist:
			content = {'Error': 'Recording not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		serializer = serializers.RecordingSerializer(recordingObj)
		return Response(serializer.data)
	
	# This should be called exclusively by the server in order to add more
	# precise informations on the recording as soon as the final encoding ends.
	# Remember to set available field to True when the file is ready in order to
	# let users see this file on their phones.
	def put(self, request, recordingID, format=None):
		# Modify recording details
		
		# Check permission
		if not request.user.has_perm('recordings.change_recording'):
			return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
		
		try:
			recordingObj = models.Recording.objects.get(recordingID=recordingID)
		except models.Recording.DoesNotExist:
			content = {'Error': 'Recording not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		ser = serializers.RecordingSerializer(recordingObj, data=request.DATA, partial=True)
		
		if ser.is_valid():
			ser.save()
			print('Recording ' + recordingID + ' updated')
			if not DISABLE_EMAIL:
				t = threading.Thread(target=emailSender, args=(recordingObj,))
				t.start()
			return Response(ser.data, status=status.HTTP_200_OK)
			
		return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
	
	# This should be called by client App in order to "forget" about this recording
	def delete(self, request, recordingID, format=None):
		# Delete current user from those authorized to access this file
		
		try:
			# This should not be fetched if it is not available yet or user have no permissions to access it
			recordingObj = models.Recording.objects.filter(users=request.user).get(recordingID=recordingID, available=True)
		except models.Recording.DoesNotExist:
			content = {'Error': 'Recording not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
			
		user = request.user
		
		recordingObj.users.remove(user)
		
		print('User ' + user.username + ' removed Recording ' + recordingID + ' from his file list')
		
		# When everyone removed this recording from their libraries
		# delete the file as well
		if not recordingObj.users.all():
			if not DISABLE_AWS_COMMANDS:
				deleteS3File(recordingObj.download_url)
			recordingObj.delete()
			print('Recording ' + recordingID + ' has no more participant and it has been deleted')
		
		return Response(status=status.HTTP_204_NO_CONTENT)

"""
# TODO: This class can be removed!
class RecordingUserList(APIView):
	permission_classes = (permissions.IsAuthenticated,)
	
	# This method is just for debug and should be restricted from client access
	def get(self, request, recordingID, format=None):
		# Print all users that have access to this file
		
		# Check permission
		if not request.user.is_superuser:
			return Response({"detail": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
		
		try:
			# This should not be fetched if it is not available yet or user have no permissions to access it
			recordingObj = models.Recording.objects.filter(users=request.user).get(recordingID=recordingID, available=True)
		except models.Recording.DoesNotExist:
			content = {'Error': 'Recording not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		users = []
		for u in recordingObj.users.all():
			username = models.UserRESTObject()
			username.username = u.username
			users.append(username)
		
		ser = serializers.UserSerializer(users, many=True)
		return Response(ser.data)
		
	# This should be called exclusively by server when adding a user that helped recording
	# in the room to the users that have access to the final file of the same room
	# TODO: This method can be useful for "file sharing" functionality: a user, owner of this file,
	# can allow another user to add it on his library.
	def post(self, request, recordingID, format=None):
		# Add user to those authorized to download this file
		
		try:
			# This should not be fetched if it is not available yet or user have no permissions to access it
			recordingObj = models.Recording.objects.filter(users=request.user).get(recordingID=recordingID, available=True)
		except models.Recording.DoesNotExist:
			content = {'Error': 'Recording not found'}
			return Response(content, status=status.HTTP_404_NOT_FOUND)
		
		ser = serializers.UserSerializer(data=request.DATA)
		
		if ser.is_valid():
			username = ser.validated_data['username']
			try:
				user = get_user_model().objects.get(username=username)
			except get_user_model().DoesNotExist:
				content = {'Error': 'User not found'}
				return Response(content, status=status.HTTP_404_NOT_FOUND)
			
			recordingObj.users.add(user)
			recordingObj.save()
			
			return Response(ser.data)
		
		return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
	# The addition of a user to the Recording he participated in moved to the
	# POST method of RoomUserList
	"""
