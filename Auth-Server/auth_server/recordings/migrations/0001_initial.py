# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Recording',
            fields=[
                ('recordingID', models.CharField(serialize=False, primary_key=True, max_length=32)),
                ('recordingName', models.CharField(null=True, blank=True, max_length=100)),
                ('start_recording', models.DateTimeField()),
                ('stop_recording', models.DateTimeField(null=True, blank=True)),
                ('length', models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=7)),
                ('dimension', models.IntegerField(null=True, blank=True)),
                ('latitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('longitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('download_url', models.URLField(null=True, blank=True)),
                ('available', models.BooleanField(default=False)),
                ('users', models.ManyToManyField(null=True, blank=True, related_name='recordings', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('roomID', models.CharField(serialize=False, primary_key=True, max_length=32)),
                ('roomName', models.CharField(null=True, blank=True, max_length=100)),
                ('start_recording', models.DateTimeField(auto_now_add=True)),
                ('stop_recording', models.DateTimeField(null=True, blank=True)),
                ('latitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('longitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('stream_url', models.URLField()),
                ('service_queue_url', models.URLField()),
                ('output_queue_url', models.URLField()),
                ('on_recording', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RoomCreationRESTObject',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('roomName', models.CharField(null=True, blank=True, max_length=100)),
                ('latitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('longitude', models.DecimalField(decimal_places=6, max_digits=9)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RoomListRESTObject',
            fields=[
                ('roomID', models.CharField(serialize=False, primary_key=True, max_length=32)),
                ('roomName', models.CharField(max_length=100)),
                ('participant', models.IntegerField()),
                ('distance', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserRESTObject',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('username', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserRoomUrl',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('user_queue_url', models.URLField(unique=True)),
                ('creation', models.DateTimeField(auto_now_add=True)),
                ('room', models.ForeignKey(to='recordings.Room')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
