from django.conf.urls import patterns, url, include
from rest_framework.authtoken import views as rest_views
from recordings import views

urlpatterns = patterns('',
	url(r'^rooms/$', views.RoomList.as_view(), name='roomlist'),
	url(r'^rooms/(?P<roomID>[a-zA-Z0-9]*)/$', views.RoomDetail.as_view(), name='roomdetail'),
	url(r'^rooms/(?P<roomID>[a-zA-Z0-9]*)/users/$', views.RoomUsersList.as_view(), name='roomuserslist'),
	url(r'^rooms/(?P<roomID>[a-zA-Z0-9]*)/users/(?P<username>[a-zA-Z@.+-_]*)/$', views.RoomUsersDetail.as_view(), name='roomusersdetail'),
	
	url(r'^recordings/$', views.RecordingList.as_view(), name='recordingslist'),
	url(r'^recordings/(?P<recordingID>[a-zA-Z0-9]*)/$', views.RecordingDetail.as_view(), name='recordingsdetail'),
	#url(r'^recordings/(?P<recordingID>[a-zA-Z0-9]*)/users/$', views.RecordingUserList.as_view(), name='recordingusers'),
)
