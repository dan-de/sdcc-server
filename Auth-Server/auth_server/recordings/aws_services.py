from django.conf import settings

import boto
from boto import sqs
from boto.sqs.message import Message
from boto.s3.connection import S3Connection, Bucket, Key

from urllib.parse import urlparse

import socket
import sys
import time

from syslog import syslog as print



sqs_conn = sqs.connect_to_region(settings.AWS_REGION)

if not boto.config.get('s3', 'use-sigv4'):
	boto.config.add_section('s3')
	boto.config.set('s3', 'use-sigv4', 'True')
s3_conn = boto.s3.connect_to_region(settings.S3_REGION)
bucket = Bucket(s3_conn, settings.S3_BUCKET_NAME)

PREFIX = 'CodeP_Queue_'

# SQS Documentation:
# 	http://boto.readthedocs.org/en/latest/sqs_tut.html
# 	http://boto.readthedocs.org/en/latest/ref/sqs.html

def getStreamUrl(roomID):
	# Returns the load balancer URL
	return settings.TCP_CLUSTER_URL

def getServiceUrl(roomID):
	# Returns the URL for the service queue of the room
	visibility_timeout = 30 # How long a message will remain invisibile to other queue readers
	                        # once it has been read
	q = sqs_conn.create_queue(PREFIX + 'service_' + roomID, visibility_timeout)
	return q.url

def getOutputUrl(roomID):
	# Returns the URL for the output queue of the room
	visibility_timeout = 30 # How long a message will remain invisibile to other queue readers
	                        # once it has been read
	q = sqs_conn.create_queue(PREFIX + 'output_' + roomID, visibility_timeout)
	return q.url

def getUserUrl(roomID, username, rand):
	username = username.replace('.','').replace('@','').replace('+','')
	# Returns the URL for the user queue of the room
	visibility_timeout = 30 # How long a message will remain invisibile to other queue readers
	                        # once it has been read
	q = sqs_conn.create_queue(PREFIX + roomID + '_user_' + username + '_' + rand, visibility_timeout)
	return q.url

def getAllQueues():
	return sqs_conn.get_all_queues(prefix=PREFIX)

def getQueueByURL(queueURL):
	return sqs.queue.Queue(connection=sqs_conn, url=queueURL)

def getQueueByName(queueName):
	return sqs.get_queue(queueName)

def deleteQueueByURL(queueURL):
	q = getQueueByURL(queueURL)
	return q.delete()

def deleteQueueByName(queueName):
	q = getQueueByName(queueName)
	return q.delete()

def sendJoinMessage(serviceURL, username, userURL):
	m = Message()
	q = getQueueByURL(serviceURL)
	
	print('Sending join message for user ' + username + ' on queue ' + serviceURL)
	m.set_body('Join message for user ' + username)
	m.message_attributes = {
							'Url': {
								'string_value': userURL,
								'data_type': 'String'
							},
							'User': {
								'string_value': username,
								'data_type': 'String'
							}
	}
	q.write(m)

def wakeupFilteringServer(serviceURL, outputURL, roomID):
	message = '{"Service": "' + serviceURL + '", "Output": "' + outputURL + '", "roomID": "' + roomID + '"}'
	print('wakeupFilteringServer: Sending wakeup message for room ' + roomID)
	
	for i in range(0,20):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # TCP/IP
		sock.settimeout(5)
		
		try:
			sock.connect(settings.FILTERING_WAKEUP_URL)
			sock.sendall(message.encode())
			print('wakeupFilteringServer: Done')
			return
		except:
			print('wakeupFilteringServer: Connection problems')
			time.sleep(5) # seconds
		finally:
			sock.close()
	print('wakeupFilteringServer: Cannot wakeup filtering server!')

def wakeupOutputServer(outputURL, roomID):
	message = '{"Output": "' + outputURL + '", "roomID": "' + roomID + '"}'
	print('wakeupOutputServer: Sending wakeup message for room ' + roomID)
	
	for i in range(0,20):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # TCP/IP
		sock.settimeout(5)
		
		try:
			sock.connect(settings.OUTPUT_WAKEUP_URL)
			sock.sendall(message.encode())
			print('wakeupOutputServer: Done')
			return
		except:
			print('wakeupOutputServer: Connection problems')
			time.sleep(5) # seconds
		finally:
			sock.close()
	print('wakeupOutputServer: Cannot wakeup saving server!')

def deleteS3File(fileURL):
	if not fileURL:
		return
	
	k = Key(bucket)
	
	url = urlparse(fileURL)
	path = url.path
	
	# If this is an S3 URL it contains references to the bucket name. Remove it
	path = path.replace(settings.S3_BUCKET_NAME + '/','')
	
	# Remove first slash
	path = path[1:]
	
	print('Deleting S3 file: ' + path)
	
	# Now delete the file
	k.key = path
	bucket.delete_key(k)
