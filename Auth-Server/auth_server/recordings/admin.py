from django.contrib import admin
from recordings import models

# Register your models here.

class RoomAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,			{'fields': ['roomID', 'roomName']}),
		('Details',		{'fields': ['start_recording', 'stop_recording', 'on_recording']}),
		('URLs',		{'fields': ['stream_url', 'service_queue_url', 'output_queue_url']}),
		('Coordinates',	{'fields': ['latitude', 'longitude']}),
	]
	
	readonly_fields = ['start_recording']
	
	list_display = ['roomID', 'roomName', 'start_recording', 'on_recording']
	search_fields = ['roomID', 'roomName']
	ordering = ['roomID', 'roomName', 'start_recording', 'on_recording']

admin.site.register(models.Room, RoomAdmin)

class UserRoomUrlAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,			{'fields': ['room', 'user', 'user_queue_url']}),
	]
	
	list_display = ['room', 'user', 'user_queue_url']
	search_fields = ['room', 'user']
	ordering = ['room', 'user']

admin.site.register(models.UserRoomUrl, UserRoomUrlAdmin)



class RecordingAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,			{'fields': ['recordingID', 'recordingName']}),
		('Users',		{'fields': ['users'], 'classes': ['collapse']}),
		('Details',		{'fields': ['start_recording', 'stop_recording', 'length', 'dimension', 'download_url', 'available']}),
		('Coordinates',	{'fields': ['latitude', 'longitude']}),
	]
	
	readonly_fields = []
	
	list_display = ['recordingID', 'recordingName', 'start_recording', 'download_url', 'length', 'dimension', 'available']
	search_fields = ['recordingID', 'recordingName', 'download_url']
	ordering = ['recordingID', 'recordingName', 'start_recording', 'length', 'dimension', 'available']

admin.site.register(models.Recording, RecordingAdmin)
