from django.db import models
from django.conf import settings

class Room(models.Model):
	# A room is a pending performance with some users currently recording
	# and sending their audio tracks to stream_url URL.
	roomID = models.CharField(max_length=32, primary_key=True)
	roomName = models.CharField(max_length=100, blank=True, null=True)
	start_recording = models.DateTimeField(auto_now_add=True)
	stop_recording = models.DateTimeField(null=True, blank=True)
	latitude = models.DecimalField(max_digits=9, decimal_places=6)
	longitude = models.DecimalField(max_digits=9, decimal_places=6)
	stream_url = models.URLField()
	service_queue_url = models.URLField()
	output_queue_url = models.URLField()
	on_recording = models.BooleanField(default=True)
	
	def __unicode__(self): # Python 2
		return 'Room ' + str(self.roomID)
	def __str__(self): # Python 3
		return 'Room ' + str(self.roomID)



class UserRoomUrl(models.Model):
	room = models.ForeignKey(Room, on_delete=models.CASCADE)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	user_queue_url = models.URLField(unique=True)
	creation = models.DateTimeField(auto_now_add=True)
	
	def __unicode__(self): # Python 2
		return 'User ' + self.user.username + 'for Room ' + str(self.room.roomID)
	def __str__(self): # Python 3
		return 'User ' + self.user.username + 'for Room ' + str(self.room.roomID)



class Recording(models.Model):
	# A recording represent a past performance saved on AWS storage
	# (S3 in the specific) and it is associated to every user that
	# participated sending their audio stream.
	# A user can remove a recording and when every user remove
	# this recording it is deleted from the cloud.
	recordingID = models.CharField(max_length=32, primary_key=True)
	recordingName = models.CharField(max_length=100, blank=True, null=True)
	users = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='recordings')
	start_recording = models.DateTimeField()
	stop_recording = models.DateTimeField(blank=True, null=True)
	length = models.DecimalField(max_digits=7, decimal_places=3, blank=True, null=True) # Represented in minutes
	dimension = models.IntegerField(blank=True, null=True) # Represented in bytes
	latitude = models.DecimalField(max_digits=9, decimal_places=6)
	longitude = models.DecimalField(max_digits=9, decimal_places=6)
	download_url = models.URLField(blank=True, null=True)
	available = models.BooleanField(default=False)
	
	def __unicode__(self): # Python 2
		return 'Recording ' + str(self.recordingID)
	def __str__(self): # Python 3
		return 'Recording ' + str(self.recordingID)



# REST dummy model objects
class RoomListRESTObject(models.Model):
	roomID = models.CharField(max_length=32, primary_key=True)
	roomName = models.CharField(max_length=100)
	participant = models.IntegerField()
	distance = models.IntegerField() # Distance in meters from the coordinates sent by the user

class RoomCreationRESTObject(models.Model):
	roomName = models.CharField(max_length=100, blank=True, null=True)
	latitude = models.DecimalField(max_digits=9, decimal_places=6)
	longitude = models.DecimalField(max_digits=9, decimal_places=6)
	
class UserRESTObject(models.Model):
	username = models.CharField(max_length=30)
