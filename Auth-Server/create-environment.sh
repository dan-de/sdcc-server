#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $DIR/environment 2>/dev/null
if [ $? != "0" ] ; then
	echo -e "Cannot import environment path" >&2
	exit 1
fi



##########################
# Checking configuration #
##########################

echo -e "Checking configuration...\n" >&2

echo -n "Checking python3... "
if [ ! `command -v python3 2>/dev/null` ] ; then
	echo -e "FAILED!\n\nPlease install python3 and retry."
	exit 1
fi
echo "OK!"

#echo -n "Checking python3-dev... "
#INCLUDE_PY=$(python3 -c "from distutils import sysconfig as s; print(s.get_config_vars()['INCLUDEPY'])")
#if [ ! -f "${INCLUDE_PY}/Python.h" ] ; then
#	echo -e "FAILED!\n\nPlease install python3-dev and retry."
#	exit 1
#fi
#echo "OK!"

echo -n "Checking virtualenv... "
if [ ! `command -v virtualenv 2>/dev/null` ] ; then
	echo -e "FAILED!\n\nPlease install virtualenv and retry."
	exit 1
fi
echo "OK!"

echo -n "Checking pip... "
if [ ! `command -v pip 2>/dev/null` ] ; then
	echo -e "FAILED!\n\nPlease install pip and retry."
	exit 1
fi
echo "OK!"

echo -n "Checking environment path... "
if [[ $VIRT_ENV =~ ^.*[[:space:]].*$ ]] ; then
	echo -e "FAILED!\n\nENVIRONMENT path must not contain spaces" >&2
	exit 1
fi
echo "OK!"



echo -ne "\nInstalling virtual environment... "

if [ `virtualenv -p python3 $VIRT_ENV >/dev/null` ] ; then
	echo -e "FAILED!\n\nCannot install virtual environment.\nIs the directory writable?" >&2
	exit 1
fi
echo "OK!"



source $VIRT_ENV/bin/activate
if [ $? != "0" ] ; then
	echo -e "Cannot access virtual environment" >&2
	exit 1
fi



#######################
# Installing packages #
#######################

# Requirements for Django
echo -ne "Installing Django... "
pip install Django >/dev/null
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Django." >&2
	exit 1
fi
echo "OK!"

# Requirements for MySQL
#echo -ne "Installing MySQL Python connector... "
#pip install MySQL-python >/dev/null
#if [ $? != "0" ] ; then
#	echo -e "FAILED!\n\nCannot install mysql-python.\nProbably you need to install libmysqlclient-dev." >&2
#	exit 1
#fi
#echo "OK!"

# Check http://saltnlight5.blogspot.it/2014/09/django-with-python-3-and-mysql-database.html
echo -ne "Installing MySQL Python connector... "
pip install --allow-all-external mysql-connector-python >/dev/null
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install mysql-python." >&2
	exit 1
fi
echo "OK!"

# Requirements for Django REST framework
echo -ne "Installing Django REST framework."
pip install djangorestframework >/dev/null
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Django REST framework." >&2
	exit 1
fi
echo -n "."
pip install markdown >/dev/null       # Markdown support for the browsable API.
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Django REST framework." >&2
	exit 1
fi
echo -n ". "
pip install django-filter >/dev/null  # Filtering support
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Django REST framework." >&2
	exit 1
fi
echo "OK!"

# Requirements for coordinates measurements
echo -ne "Installing Geopy... "
pip install geopy >/dev/null
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Geopy." >&2
	exit 1
fi
echo "OK!"

# Requirements for Boto
echo -ne "Installing Boto (AWS SDK)... "
pip install boto >/dev/null
if [ $? != "0" ] ; then
	echo -e "FAILED!\n\nCannot install Boto." >&2
	exit 1
fi
echo "OK!"



#####################
# Setting up Django #
#####################

echo -e "\nSetting up Django..."
python3 $DJANGO_PROJ/manage.py migrate > /dev/null
if [ $? != "0" ] ; then
	echo -e "Error synchronizing database. Check manually." >&2
	exit 1
fi

read -p "Do you want to create a new superuser [Y|n]? " -n 1 -r
echo
if [[ $REPLY =~ ^[Nn]$ ]]
then
	echo "You can always create a superuser using script create-superuser.sh"
else
	echo "Creating superuser..."
	python3 $DJANGO_PROJ/manage.py createsuperuser
	if [ $? != "0" ] ; then
		echo -e "Error creating superuser" >&2
		exit 1
	fi
fi

echo -e "\nEND"

exit 0
